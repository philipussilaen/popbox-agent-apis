<?php

namespace App\Http\Middleware;

use App\Models\Company;
use App\Models\Log;
use App\Models\Privilege;
use App\Models\UserSession;
use App\User;
use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // create default response
        $status = 200;
        $message = null;
        $response = new \stdClass();
        $response->code = $status;
        $response->message = $message;

        // check if request is json
        if (!$request->hasHeader('Content-Type') || $request->header('Content-Type')!='application/json'){
            $status = 400;
            $message = 'Invalid Content-Type';

            $result = new \stdClass();
            $response->code = $status;
            $response->message = $message;
            $result->response = $response;
            $result->data = [];
            return response()->json($result);
        }

        // get token
        $token = $request->input('token',null);
        if (empty($token)){
            $status = 400;
            $message = 'Missing Token';

            $result = new \stdClass();
            $response->code = $status;
            $response->message = $message;
            $result->response = $response;
            $result->data = [];
            return response()->json($result);
        }

        // check token
        $checkToken = Company::apiCheckToken($token);
        if (!$checkToken){
            $status = 403;
            $message = 'Invalid Token';

            $result = new \stdClass();
            $response->code = $status;
            $response->message = $message;
            $result->response = $response;
            $result->data = [];
            return response()->json($result);
        }

        $moduleName = $request->path();
        $ignoreUsername = ['api/agent/login','api/agent/register','api/agent/forgot','api/agent/check','api/agent/checkpassword','api/agent/reset','api/agent/activate','api/referral/check','api/locker/detail',
            'api/partner/transaction/create', 'api/partner/transaction/products', 'api/service/message/send', 'api/service/message/store-file',];
        $userId = null;
        if (!in_array($moduleName,$ignoreUsername)){
            // get username
            $username = $request->input('username',null);
            if (empty($username)){
                $status = 400;
                $message = 'Missing Username';

                $result = new \stdClass();
                $response->code = $status;
                $response->message = $message;
                $result->response = $response;
                $result->data = [];
                return response()->json($result);
            }

            // get user Db
            $userDb = User::where('username',$username)->first();
            if (!$userDb){
                $status = 403;
                $message = 'Invalid Username';

                $result = new \stdClass();
                $response->code = $status;
                $response->message = $message;
                $result->response = $response;
                $result->data = [];
                return response()->json($result);
            }
            $userId = $userDb->id;

            $sessionId = $request->input('session_id',null);
            if (empty($sessionId)){
                $status = 401;
                $message = 'Sesi Anda telah habis. Silahkan Login/Masuk kembali';

                $result = new \stdClass();
                $response->code = $status;
                $response->message = $message;
                $result->response = $response;
                $result->data = [];
                return response()->json($result);
            }

            // validate sessions
            $checkUserSession = UserSession::checkUserSession($userId,$sessionId);
            if (!$checkUserSession->isSuccess){
                $status = 401;
                $message = $checkUserSession->errorMsg;

                $result = new \stdClass();
                $response->code = $status;
                $response->message = $message;
                $result->response = $response;
                $result->data = [];
                return response()->json($result);
            }
        }

        // log to DB
        $ignoredLog = ['api/service/getPopShopProduct'];
        $logId = null;
        if(!in_array($moduleName,$ignoredLog)) {
            $logId = Log::insertRequest($userId,$moduleName,$request);
        }
        // process
        $response =  $next($request);

        if (!in_array($moduleName,$ignoredLog) && !empty($logId)){
            Log::updateResponse($logId,$response);
        }
        return $response;
    }
}
