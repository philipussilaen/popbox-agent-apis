<?php

namespace App\Http\Middleware;

use Closure;

class ApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // set start time
        $startTime = microtime(true);
        // get token
        $token = $request->input('token',null);
        // get url
        $url = $request->path();
        // get IP
        $ip = $request->ip();
        //get param
        $param = json_encode($request->except('token'));
        $this->log("$token $ip $url $param");

        $response = $next($request);
        // calculate duration
        $endTime = microtime(true);
        $duration = ($endTime - $startTime);

        $originalResponse = $response->original;
        if (!isset($originalResponse->response->latency)){
            if (is_array($originalResponse)) $originalResponse['response']['latency'] = $duration;
            else $originalResponse->response->latency = $duration;
            $originalResponse = json_encode($originalResponse);
            $response->setContent($originalResponse);
        };

        $logResponse = ($response->content());
        $this->log("$token $duration $logResponse");

        return $response;
    }

    /**
     * log to file every request
     * @param string $msg
     */
    private function log($msg=''){
        $msg = " $msg\n";
        $f = fopen(storage_path().'/logs/access/'.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
    }
}
