<?php

namespace App\Http\Middleware;

use Closure;

class ConvertToEmptyString
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response =  $next($request);
        $dataResponse = $response->getContent();
        if ($this->isJSON($dataResponse)){
            $dataArray = json_decode($dataResponse,true);
            $dataArray = $this->replaceNullWithEmptyString($dataArray);
            $dataJson = json_encode($dataArray);
            $response = $response->setContent($dataJson);
        }
        return $response;
    }

    function replaceNullWithEmptyString($array)
    {
        foreach ($array as $key => $value)
        {
            if(is_array($value)){
                $array[$key] = $this->replaceNullWithEmptyString($value);
            }
            else
            {
                if (is_null($value)){
                    $array[$key] = "";
                }
            }
        }
        return $array;
    }

    function isJSON($string){
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}
