<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 24/10/2017
 * Time: 07.48
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class ApiPopExpress
{
    private $id = null;
    /**
     * @param string $request
     * @param array $param
     * @return mixed
     */
    private function cUrl($request, $param = array()){
        if (empty($this->id)) $this->id = uniqid();
        $unique = $this->id;

        $host = env('POP_EXPRESS_URL');
        $token = env('POP_EXPRESS_KEY');
        $header = [];
        $header[] = 'Content-Type:application/json';
        $header[] = 'api-key:'.$token;

        $url = $host.'/'.$request;
        $param['api_key'] = $token;
        $json = json_encode($param);

        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg = "$unique > $time Request : $url : $json\n";
        $f = fopen(storage_path().'/logs/api/express.'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        $ch = curl_init();
        // 2. set the options, including the url
        curl_setopt($ch, CURLOPT_URL,           $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_POST,           count($param));
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $json );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        $output = curl_exec($ch);
        curl_close($ch);

        $time = date('H:i:s');
        $msg = "$unique > $time Response : $output\n";
        $f = fopen(storage_path().'/logs/api/express.'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $json,
                'api_response'  => $output,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        return $output;
    }

    /**
     * Get City Origin
     * @return mixed
     */
    public function getCityOrigin(){
        $url = 'list/origin';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get Province Destination
     * @return mixed
     */
    public function getProvinceDestination(){
        $url = 'list/province';
        $param = [];
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get City Destination
     * @param $provinceName
     * @return mixed
     */
    public function getCityDestination($provinceName){
        $url = 'list/county';
        $param = [];
        $param['province'] = $provinceName;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get District Destination
     * @param $cityName
     * @return mixed
     */
    public function getDistrictDestination($cityName){
        $url = 'list/district';
        $param = [];
        $param['county'] = $cityName;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * Get Tariff
     * @param $origin
     * @param $originCode
     * @param $province
     * @param $city
     * @param $district
     * @return mixed
     */
    public function getTariff($origin,$originCode,$province,$city,$district){
        $url = 'tariff';
        $param = [];
        $param['origin'] = $origin;
        $param['origin_code'] = $originCode;
        $param['destination']['province'] = $province;
        $param['destination']['county'] = $city;
        $param['destination']['district'] = $district;
        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        return $result;
    }

    /**
     * @param $agentName
     * @param $pickupDate
     * @param $pickupTime
     * @param $pickupLocation
     * @param array $paramData
     * @return \stdClass
     */
    public function calculatePickup($agentName,$pickupDate,$pickupTime,$pickupLocation,$paramData=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $required = ['order_no','sender_telp','weight','service_type','recipient_address','recipient_telp','recipient_name','origin','destination_province','destination_county','destination_district','insurance'];
        // get all param
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$paramData)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        $url = 'pickup_request/popagent/calc';
        $param = [];
        $param['account_name'] = $agentName;
        $param['pickup_date'] = $pickupDate;
        $param['pickup_time'] = $pickupTime;
        $param['pickup_location'] = $pickupLocation;
        $param['data'][] = $paramData;

        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        $response->isSuccess = true;
        $response->data = $result;
        return $response;
    }

    /**
     * Submit / Create Order
     * @param $branchId
     * @param $agentName
     * @param $pickupDate
     * @param $pickupTime
     * @param $pickupLocation
     * @param array $paramData
     * @return \stdClass
     */
    public function submitPickup($branchId,$agentName,$pickupDate,$pickupTime,$pickupLocation,$paramData=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $required = ['order_no','sender_telp','weight','service_type','recipient_address','recipient_telp','recipient_name','origin','destination_province','destination_county','destination_district','insurance'];
        // get all param
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$paramData)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $response->errorMsg = $message;
            return $response;
        }

        $url = 'pickup_request/popagent/create';
        $param = [];
        $param['branch_id'] = $branchId;
        $param['account_name'] = $agentName;
        $param['pickup_date'] = $pickupDate;
        $param['pickup_time'] = $pickupTime;
        $param['pickup_location'] = $pickupLocation;
        $param['data'][] = $paramData;

        $result = $this->cUrl($url,$param);
        $result = json_decode($result);
        $response->isSuccess = true;
        $response->data = $result;
        return $response;
    }
}