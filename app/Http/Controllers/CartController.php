<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Cart;
use App\Models\CartItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    // cart generate
    /**
     * check / get existing cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkCart(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // get username
        $username = $request->input('username');
        // get cart data
        $cartDb = Cart::getCarts($username);
        if (!$cartDb->isSuccess){
            $message = $cartDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $data = $cartDb->data;
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * insert new items to current cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function insertItem(Request $request){
        $required = ['item_name','item_type','item_price','item_param'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // get variable
        $username = $request->input('username');
        $itemName = $request->input('item_name');
        $itemType = $request->input('item_type');
        $itemPrice = $request->input('item_price');
        $itemPicture = $request->input('item_picture',null);
        $itemParam = $request->input('item_param');

        DB::beginTransaction();
        // get cart id
        $cartDb = Cart::getCarts($username);
        if (!$cartDb->isSuccess){
            // if not exist create new cart
            $cartDb = Cart::createCart($username);
            if (!$cartDb->isSuccess){
                DB::rollback();
                $message = "Failed to Create Cart";
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }
        // get cart id
        $cartId = $cartDb->cartId;
        // insert into cart Items
        $itemDb = CartItem::insertItem($cartId,$itemName,$itemType,$itemPrice,$itemParam,$itemPicture);
        if (!$itemDb->isSuccess){
            DB::rollback();
            $message = $itemDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::commit();

        // generate response with new cart data
        $cartDb = Cart::getCarts($username);
        $data = $cartDb->data;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * remove item from cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeItem(Request $request){
        $required = ['item_id'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // retrieve variable
        $itemId = $request->input('item_id');
        $username = $request->input('username');

        DB::beginTransaction();

        // check cart first
        $cartDb = Cart::getCarts($username);
        if (!$cartDb->isSuccess){
            DB::rollback();
            $message = $cartDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // delete item from cart
        $itemDb = CartItem::removeItem($itemId);
        if (!$itemDb->isSuccess){
            DB::rollback();
            $message = $itemDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get cart items
        $cartDb = Cart::getCarts($username);
        if (!$cartDb->isSuccess){
            DB::rollback();
            $message = $cartDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $cartDb->data;
        // if empty items
        if (empty($data->items)){
            // update cart to 0
            Cart::removeCart($username);
        }

        DB::commit();

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }

    /**
     * update item from cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateItem(Request $request){
        $required = ['item_id'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        // retrieve variable
        $itemId = $request->input('item_id');
        $amount = $request->input('amount');
        $item_price = $request->input('item_price');
        $username = $request->input('username');
        
        DB::beginTransaction();
        
        // check cart first
        $cartDb = Cart::getCarts($username);
        if (!$cartDb->isSuccess){
            DB::rollback();
            $message = $cartDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        // update item in cart
        $itemDb = CartItem::updateItem($itemId, $amount, $item_price);
        if (!$itemDb->isSuccess){
            DB::rollback();
            $message = $itemDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        // get cart items
        $cartDb = Cart::getCarts($username);
        if (!$cartDb->isSuccess){
            DB::rollback();
            $message = $cartDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $data = $cartDb->data;
        // if empty items
        if (empty($data->items)){
            // update cart to 0
            Cart::removeCart($username);
        }
        
        DB::commit();
        
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }
    public function updateItems(Request $request){
        $required = ['items'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        // retrieve variable
        $items = $request->input('items');
        $username = $request->input('username');

        if(sizeof($items) == 0) {
            $message = "Parameter items not found";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        
        foreach ($items as $item){
            $itemId = $item['item_id'];
            $amount = $item['amount'];
            $item_price = $item['item_price'];
            
            DB::beginTransaction();
            
            // check cart first
            $cartDb = Cart::getCarts($username);
            if (!$cartDb->isSuccess){
                DB::rollback();
                $message = $cartDb->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            
            // update item in cart
            $itemDb = CartItem::updateItem($itemId, $amount, $item_price);
            if (!$itemDb->isSuccess){
                DB::rollback();
                $message = $itemDb->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            
            // get cart items
            $cartDb = Cart::getCarts($username);
            if (!$cartDb->isSuccess){
                DB::rollback();
                $message = $cartDb->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }

            $data = $cartDb->data;
            // if empty items
            if (empty($data->items)){
                // update cart to 0
                Cart::removeCart($username);
            }
                
            DB::commit();
        }
                
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }
    /**
     * check purchase / get minimum purchase at cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPurchase(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // get username
        $username = $request->input('username');

        $userDb = User::with(['virtualLocker','virtualLocker.city','virtualLocker.city.region'])
            ->where('username',$username)
            ->first();

        if (!$userDb){
            $resp=['response' => ['code' => 400,'message' => 'User Not Valid'], 'data' => []];
            return response()->json($resp);
        }
    
//         $userType = $userDb->virtualLocker->type;
        $minPurchase = $userDb->virtualLocker->city->region->min_purchase;

//         if($userType != 'warung') {
//             $resp=['response' => ['code' => 400,'message' => 'Usertype Must be Warung'], 'data' => []];
//             return response()->json($resp);
//         }

        $cartDb = Cart::getCarts($username);
        $totalPrice = 0;

        foreach($cartDb->data->items as $item) {
            if ($item->type == 'popshop'){
                $params = json_decode($item->params);
                if (isset($params->sku)){
                    $totalPrice += $item->price;
                }
            }
        }
        
        \Log::debug($totalPrice. ' - ' . $minPurchase);
        
        if ($totalPrice <= $minPurchase){
            $minPurchase = number_format($minPurchase,2,',','.');
            $resp=['response' => ['code' => 400,'message' => 'Nominal transaksi belum mencapai jumlah minimum Rp ' . $minPurchase], 'data' => []];
            return response()->json($resp);
        }

        $data = $cartDb->data;
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$data]];
        return response()->json($resp);
    }
}
