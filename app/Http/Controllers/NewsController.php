<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function getNews(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get active
        $nowDate = date('Y-m-d H:i:s');
        $articlesDb = Article::where('status',1)
            ->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->get();

        $newsList = [];
        foreach ($articlesDb as $item){
            $tmp = new \stdClass();
            $tmp->promo_id = $item->id;
            $tmp->type = $item->type;
            $tmp->value = $item->value;
            $tmp->title_header = $item->title;
            $tmp->image_header = asset('articles')."/".$item->image;
            $newsList[] = $tmp;
        }
        if (empty($newsList)){
            $message = "Empty News/Promo";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$newsList]];
        return response()->json($resp);
    }
}
