<?php

namespace App\Http\Controllers;

use App\Http\Helpers\WebCurl;
use App\Models\AgentUser;
use App\Models\Locker;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VirtualLockerController extends Controller
{
    var $curl;

    public function __construct() {
        $headers    = ['Content-Type: application/json'];
        $this->curl = new WebCurl($headers);
    }

    // function from old app
    protected $headers = array (
        'Content-Type: application/json'
    );
    protected $is_post = 0;

    /**
     * ========================= Base Function  ======================
     */

    private function post_data($url, $post_data = array(), $headers = array(), $options = array()) {
        $result = null;
        $curl = curl_init ();

        if ((is_array ( $options )) && count ( $options ) > 0) {
            $this->options = $options;
        }
        if ((is_array ( $headers )) && count ( $headers ) > 0) {
            $this->headers = $headers;
        }
        if ($this->is_post !== null) {
            $this->is_post = 1;
        }

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLOPT_POST, $this->is_post );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $post_data );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $this->headers );

        $content = curl_exec ( $curl );
        $response = curl_getinfo ( $curl );
        $result = json_decode ( $content, TRUE );

        curl_close ( $curl );
        return $result;
    }

    private function postApi($url,$params){
        $url = env('VL_URL','http://locker.dev/api/').$url;
        $params['token'] = env('VL_TOKEN','79XM983RH8TK37KTR84NNUCYK8DTLTRDXE5Z77UP');

        $headers = array (
            'Content-Type: application/json'
        );
        $result = null;
        $curl = curl_init ();
        $params = json_encode($params);

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLINFO_HEADER_OUT, true);
        curl_setopt ( $curl, CURLOPT_POST, 1 );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $params );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $headers );

        $content = curl_exec ( $curl );
        $response = curl_getinfo ( $curl );

        curl_close ( $curl );

        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => $params,
                'api_response'  => $content,
                'response_date'     => date("Y-m-d H:i:s")
            ]);

        return $content;
    }

    /**
     * ====================== End Base Function  ======================
     */

    /**
     * -------------- Public Function  --------------
     */

    public function registerVirtualLocker(Request $request){
        //required param list
        $required = ['locker_name','address','city_id','latitude','longitude','picture','open_hour','close_hour','username','days','services'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // check username first
        $checkAgent = User::where('username',$input['username'])->first();
        if (!$checkAgent){
            $message = "Agent Not Found";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // validate days, open_hour, close hour
        $days = $input['days'];
        $openHour = $input['open_hour'];
        $closeHour = $input['close_hour'];
        if (!is_array($days)){
            $resp=['response' => ['code' => 400,'message' =>'Days must be array'], 'data' => []];
            return response()->json($resp);
        }
        if (!is_array($openHour)){
            $resp=['response' => ['code' => 400,'message' =>'Open hour must be array'], 'data' => []];
            return response()->json($resp);
        }
        if (!is_array($closeHour)){
            $resp=['response' => ['code' => 400,'message' =>'Close Hour must be array'], 'data' => []];
            return response()->json($resp);
        }

        // create parameter to push to virtual locker api
        $params=[];
        $params['locker_name'] = $input['locker_name'];
        $params['address'] = $input['address'];
        $params['detail_address'] = empty($input['detail_address']) ? null : $input['detail_address'];
        $params['city_id'] = $input['city_id'];
        $params['picture'] = $input['picture'];
        $params['open_hour'] = $openHour;
        $params['close_hour'] = $closeHour;
        $params['days'] = $days;
        $params['lat'] = $input['latitude'];
        $params['long'] = $input['longitude'];
        $params['username'] = "agent_".$input['username'];
        $params['services'] = $input['services'];

        // post to API Virtual Locker
        $url = 'locker/insert';
        $postApi = $this->postApi($url,$params);
        $postApi = json_decode($postApi);

        if (!$postApi || $postApi->response->code!=200){
            $message =  $postApi->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get Locker data
        $lockerData = $postApi->data[0];
        $lockerId = $lockerData->locker_id;

        // update data user
        $agentDb = User::where('username',$input['username'])->first();
        $agentDb->locker_id = $lockerId;
        $agentDb->save();

        /*Save to Locker DB*/
        $lockerDb = Locker::where('id',$lockerId)->first();
        if (!$lockerDb){
            $lockerDb = new Locker();
            $lockerDb->id = $lockerId;
            $lockerDb->balance = 0;
            $lockerDb->save();
        }
        $response = new \stdClass();
        $response->name = $agentDb->name;
        $response->phone = $agentDb->phone;
        $response->email = $agentDb->email;
        $response->username = $agentDb->username;
        $response->locker_id = $agentDb->locker_id;
        $response->status = $agentDb->status;
        $tmp[] = $response;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $tmp];
        return response()->json($resp);
    }

    /**
     * ------------- End Public Function -------------
     */
}
