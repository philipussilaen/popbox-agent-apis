<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ApiPopExpress;
use App\Http\Helpers\ApiVirtualLocker;
use App\Models\CommissionSchema;
use App\Models\DeliveryCity;
use App\Models\DeliveryDistrict;
use App\Models\DeliveryOrder;
use App\Models\DeliveryProvince;
use App\Models\Locker;
use App\Models\Payment;
use App\Models\SmsNotification;
use App\Models\Transaction;
use App\Models\TransactionItem;
use App\Models\VirtualLocker;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DeliveryController extends Controller
{
    /**
     * Get City Origin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCityOrigin(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $username = $request->input('username');
        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = 'User Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerId = $userDb->locker_id;
        // get VirtualLocker
        $virtualLockerDb = VirtualLocker::with('city')
            ->where('locker_id',$lockerId)
            ->first();
        if (!$virtualLockerDb){
            $message = 'Locker Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerCity = $virtualLockerDb->city->city_name;

        // get city origin from
        $cityOrigin = cache('express-cityOrigin',null);
        if (empty($cityOrigin)){
            // get from API
            $popExpressAPI = new ApiPopExpress();
            $getCity = $popExpressAPI->getCityOrigin();
            if (empty($getCity)){
                $message = 'Failed Get City';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            if ($getCity->response->code !=200){
                $message = $getCity->response->message;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $cityOrigin = $getCity->data->origin;
            cache(['express-cityOrigin'=>$cityOrigin],Carbon::now()->addWeek(1));
        }

        if (empty($cityOrigin)){
            $message = 'Empty City Origin';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => $cityOrigin];
        return response()->json($resp);
    }

    /**
     * Get Province Destination
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProvinceDestination(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get province destination
        $provinceDestination = cache('express-provinceDestination',null);
        if (empty($provinceDestination)){
            // get from API
            $popExpressAPI = new ApiPopExpress();
            $getCity = $popExpressAPI->getProvinceDestination();
            if (empty($getCity)){
                $message = 'Failed Get City';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            if ($getCity->response->code !=200){
                $message = $getCity->response->message;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $provinceDestination = $getCity->data->province;
            cache(['express-provinceDestination'=>$provinceDestination],Carbon::now()->addWeek(1));
        }

        if (empty($provinceDestination)){
            $message = 'Empty City Origin';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' =>$provinceDestination];
        return response()->json($resp);
    }

    /**
     * Get City Destination
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCityDestination(Request $request){
        //required param list
        $required = ['province_name'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get province destination
        $provinceName = $request->input('province_name');
        $tmpProvinceName = trim($provinceName," ");
        $cityDestination = cache('express-cityDestination-'.$tmpProvinceName,null);
        if (empty($cityDestination)){
            // get from API
            $popExpressAPI = new ApiPopExpress();
            $getCity = $popExpressAPI->getCityDestination($provinceName);
            if (empty($getCity)){
                $message = 'Failed Get City';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            if ($getCity->response->code !=200){
                $message = $getCity->response->message;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $cityDestination = $getCity->data->counties;
            cache(['express-cityDestination-'.$tmpProvinceName=>$cityDestination],Carbon::now()->addWeek(1));
        }

        if (empty($cityDestination)){
            $message = 'Empty City Origin';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' =>$cityDestination];
        return response()->json($resp);
    }

    /**
     * Get District Destination
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistrictDestination(Request $request){
        //required param list
        $required = ['city_name'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get province destination
        $cityName = $request->input('city_name');
        $tmpCityName = trim($cityName," ");
        $districtDestination = cache('express-districtDestination-'.$tmpCityName,null);
        if (empty($districtDestination)){
            // get from API
            $popExpressAPI = new ApiPopExpress();
            $getCity = $popExpressAPI->getDistrictDestination($cityName);
            if (empty($getCity)){
                $message = 'Failed Get City';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            if ($getCity->response->code !=200){
                $message = $getCity->response->message;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $districtDestination = $getCity->data->districts;
            cache(['express-districtDestination-'.$tmpCityName=>$districtDestination],Carbon::now()->addWeek(1));
        }

        if (empty($districtDestination)){
            $message = 'Empty District Destination';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' =>$districtDestination];
        return response()->json($resp);
    }

    /**
     * Get Destination in One API
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDestination(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $list = [];
        $dataDb = DeliveryProvince::with('cities.districts')->get();

        foreach ($dataDb as $province){
            $tmp = new \stdClass();
            $tmp->province_name = $province->province_name;
            $cityList = [];
            $cityDb = $province->cities;
            foreach ($cityDb as $city){
                $tmpCity = new \stdClass();
                $tmpCity->city_name = $city->city_name;
                $districtList = [];
                $districtDb = $city->districts;
                foreach ($districtDb as $district) {
                    $tmpDistrict = new \stdClass();
                    $tmpDistrict->code = $district->code;
                    $tmpDistrict->district_name = $district->district_name;
                    $districtList[] = $tmpDistrict;
                }
                $tmpCity->districts = $districtList;
                $cityList[] = $tmpCity;
            }
            $tmp->cities = $cityList;
            $list[] = $tmp;
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => $list];
        return response()->json($resp);
    }

    /**
     * Check Tariff
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getCheckTariff(Request $request){
        //required param list
        $required = ['district_code','weight'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $districtCode = $request->input('district_code');
        $weight = $request->input('weight');

        // get district Db
        $districtDb = DeliveryDistrict::where('code',$districtCode)->first();
        if (!$districtDb){
            $message = "Invalid District Code";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $username = $request->input('username');
        $userDb = User::where('phone',$username)->first();
        $lockerId = $userDb->locker_id;

        // get VirtualLocker
        $virtualLockerDb = VirtualLocker::with('city')
            ->where('locker_id',$lockerId)
            ->first();
        if (!$virtualLockerDb){
            $message = 'Locker Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerCity = $virtualLockerDb->city->city_name;
        $lockerName = $virtualLockerDb->locker_name;

        $cityOrigin = cache('express-cityOrigin',null);
        if (empty($cityOrigin)){
            // get from API
            $popExpressAPI = new ApiPopExpress();
            $getCity = $popExpressAPI->getCityOrigin();
            if (empty($getCity)){
                $message = 'Failed Get City';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            if ($getCity->response->code !=200){
                $message = $getCity->response->message;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $cityOrigin = $getCity->data->origins;
            cache(['express-cityOrigin'=>$cityOrigin],Carbon::now()->addWeek(1));
        }
        if (empty($cityOrigin)){
            $message = 'Empty City Origin';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $isOrigin = null;
        $originCode = null;
        foreach ($cityOrigin as $item) {
            $tmpName = strtoupper($item->name);
            if (strpos($lockerCity,$tmpName) !== false) {
                $isOrigin = $item->name;
                $originCode = $item->code;
            }
        }
        if (empty($isOrigin)){
            $message = 'Out of Coverage';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $origin = $isOrigin;
        $province = $districtDb->city->province->province_name;
        $city = $districtDb->city->city_name;
        $district = $districtDb->district_name;

        $apiPopExpress = new ApiPopExpress();
        $postTariff = $apiPopExpress->getTariff($origin,$originCode,$province,$city,$district);

        if (empty($postTariff)){
            $message = "Failed Post Tariff";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($postTariff->response->code != 200){
            $message = $postTariff->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $tariffData = $postTariff->data->tariffs[0];
        $tariffList = [];
        foreach ($tariffData as $index => $item){
            $type = $item->type;
            $price = 0;
            $estimated = '';
            // hit calculate
            $data = [];
            $data['order_no'] = 'AGN_TARIFF';
            $data['sender_telp'] = $username;
            $data['notes'] = '';
            $data['weight'] = $weight;
            $data['service_type'] = $type;
            $data['recipient_address'] = 'Address';
            $data['recipient_telp'] = 'phone';
            $data['recipient_name'] = 'name';
            $data['origin'] = $isOrigin;
            $data['destination_province'] = $districtDb->city->province->province_name;
            $data['destination_county'] = $districtDb->city->city_name;
            $data['destination_district'] = $districtDb->district_name;
            $data['destination_zipcode'] = '';
            $data['parcel_price'] = '';
            $data['insurance'] = 0;
            $data['reseller'] = $virtualLockerDb->locker_name;

            $pickupDate = date('Y-m-d');
            $pickupTime = "17.00 to 21.00";
            $pickupLocation = $virtualLockerDb->address;

            $calculate = $apiPopExpress->calculatePickup($lockerName,$pickupDate,$pickupTime,$pickupLocation,$data);
            if (!empty($calculate)){
                if ($calculate->isSuccess){
                    $result = $calculate->data;
                    if (!empty($result)){
                        if ($result->response->code== 200){
                            $resultData = $result->data;
                            $price = $resultData->total_price;
                        }
                    }
                }
            }
            $estimated = $item->estimation;
            $tmp = new \stdClass();
            $tmp->type = $type;
            $tmp->price = $price;
            $tmp->estimated = $estimated;
            $tariffList[] = $tmp;
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => $tariffList];
        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getCalculatePickup(Request $request){
        //required param list
        $required = ['recipient_district_code','weight','service_type','is_insurance'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $senderPhone = $request->input('sender_phone','phone');
        $recipientName = $request->input('recipient_name','name');
        $recipientAddress = $request->input('recipient_address','address');
        $recipientPhone = $request->input('recipient_phone','phone');
        $recipientDistrictCode = $request->input('recipient_district_code');

        $awbNumber = $request->input('awb_number',null);
        $weight = $request->input('weight');
        $serviceType = $request->input('service_type');
        $isInsurance = $request->input('is_insurance',0);
        $parcelPrice = $request->input('parcel_price');
        $notes = $request->input('notes');

        $username = $request->input('username');

        // validate input
        $rules = [
            "recipient_district_code" => 'required',
            "weight" => 'required|numeric',
            "service_type" => 'required|in:regular,one_day',
            "is_insurance" => 'required|in:0,1',
            "parcel_price" => 'numeric'
        ];
        // make validation
        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $message = implode(' ',$errors);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // check AWB Number
        if (!empty($awbNumber)){
            $deliveryOrderDb = DeliveryOrder::where('awb_number',$awbNumber)->first();
            if ($deliveryOrderDb){
                $message = 'AWB Number already Used';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }

        // get user db
        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = "Invalid User";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerId = $userDb->locker_id;

        // get virtual locker
        $virtualLockerDb = VirtualLocker::with('city')
            ->where('locker_id',$lockerId)
            ->first();
        if (!$virtualLockerDb){
            $message = 'Locker Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerCity = $virtualLockerDb->city->city_name;

        $cityOrigin = cache('express-cityOrigin',null);
        if (empty($cityOrigin)){
            // get from API
            $popExpressAPI = new ApiPopExpress();
            $getCity = $popExpressAPI->getCityOrigin();
            if (empty($getCity)){
                $message = 'Failed Get City';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            if ($getCity->response->code !=200){
                $message = $getCity->response->message;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $cityOrigin = $getCity->data->origins;
            cache(['express-cityOrigin'=>$cityOrigin],Carbon::now()->addWeek(1));
        }
        if (empty($cityOrigin)){
            $message = 'Empty City Origin';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $isOrigin = null;
        $originCode = null;
        foreach ($cityOrigin as $item) {
            $tmpName = strtoupper($item->name);
            if (strpos($lockerCity,$tmpName) !== false) {
                $isOrigin = $item->name;
                $originCode = $item->code;
            }
        }
        if (empty($isOrigin)){
            $message = 'Out of Coverage';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get recipient address detail
        $deliveryDistrictDb = DeliveryDistrict::where('code',$recipientDistrictCode)->first();
        if (!$deliveryDistrictDb){
            $message = "Invalid District Code";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // create parameter for PopExpress
        $lockerName = $virtualLockerDb->locker_name;
        $pickupDate = date('Y-m-d');
        $pickupTime = "17.00 to 21.00";
        $pickupLocation = $virtualLockerDb->address;
        if (empty($awbNumber)) $awbNumber = 'AWB_CALC';
        $data = [];
        $data['order_no'] = $awbNumber;
        $data['sender_telp'] = $senderPhone;
        $data['notes'] = $notes;
        $data['weight'] = $weight;
        $data['service_type'] = $serviceType;
        $data['recipient_address'] = $recipientAddress;
        $data['recipient_telp'] = $recipientPhone;
        $data['recipient_name'] = $recipientName;
        $data['origin'] = $isOrigin;
        $data['origin_code'] = $originCode;
        $data['destination_province'] = $deliveryDistrictDb->city->province->province_name;
        $data['destination_county'] = $deliveryDistrictDb->city->city_name;
        $data['destination_district'] = $deliveryDistrictDb->district_name;
        $data['destination_zipcode'] = '';
        $data['parcel_price'] = $parcelPrice;
        $data['insurance'] = $isInsurance;
        $data['reseller'] = $virtualLockerDb->locker_name;

        // push to popexpress
        $apiPopExpress = new ApiPopExpress();
        $calculate = $apiPopExpress->calculatePickup($lockerName,$pickupDate,$pickupTime,$pickupLocation,$data);
        if (empty($calculate)){
            $message = "Failed Calculate";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if (!$calculate->isSuccess){
            $message = $calculate->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $result = $calculate->data;
        if (empty($result)){
            $message = "Failed Calculate";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($result->response->code!= 200){
            $message = $result->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $resultData = $result->data;
        $totalPrice = (int)$resultData->total_price;
        // Calculate Schema
        $basicPrice = $totalPrice;
        $publishPrice = $totalPrice;
        $params = [];
        $params['transaction_type'] = 'delivery';
        $params['transaction_item_type'] = 'delivery';
        $params['user_id'] = $userDb->id;
        $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);

        $subTotalPrice = $resultData->weight * $resultData->price_per_kg;

        $response = new \stdClass();
        $response->origin_city = $isOrigin;
        $response->destination_province = $data['destination_province'];
        $response->destination_city = $data['destination_county'];
        $response->destination_district = $data['destination_district'];
        $response->service_type = $resultData->service_type;
        $response->weight = $resultData->weight;
        $response->actual_weight = $weight;
        $response->price_per_kg = $resultData->price_per_kg;
        $response->insurance_price = (int)$resultData->insurance_price;
        $response->sub_total_price = $subTotalPrice;
        $response->total_price = $totalPrice;
        $response->commission = $commission->commission;

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$response]];
        return response()->json($resp);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function postSubmitPickup(Request $request){
        //required param list
        $required = ['sender_name','sender_phone','recipient_name','recipient_address','recipient_phone','recipient_district_code','awb_number','weight','service_type','is_insurance','agent_password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $senderName = $request->input('sender_name');
        $senderPhone = $request->input('sender_phone');
        $senderEmail = $request->input('sender_email',null);
        $recipientName = $request->input('recipient_name');
        $recipientAddress = $request->input('recipient_address');
        $recipientPhone = $request->input('recipient_phone');
        $recipientEmail = $request->input('recipient_email',null);
        $recipientDistrictCode = $request->input('recipient_district_code');

        $awbNumber = $request->input('awb_number');
        $weight = $request->input('weight');
        $serviceType = $request->input('service_type');
        $isInsurance = $request->input('is_insurance',0);
        $parcelPrice = $request->input('parcel_price',0);
        $notes = $request->input('notes',"");
        $reseller = $request->input('reseller_name');

        $username = $request->input('username');
        $agentPassword = $request->input('agent_password');

        // validate AWB
        $checkDelivery = DeliveryOrder::where('awb_number',$awbNumber)->first();
        if ($checkDelivery){
            $message = "AWB Number already Exist";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // validate input
        $rules = [
            "sender_name"  => 'required|regex:/^[\pL\s\-]+$/u',
            "sender_phone" => 'required|numeric|digits_between:10,14',
            "sender_email" => 'nullable|email',
            "recipient_name" => 'required|regex:/^[\pL\s\-]+$/u',
            "recipient_address" => 'required',
            "recipient_phone" => 'required|numeric|digits_between:10,14',
            "recipient_email" => 'nullable|email',
            "recipient_district_code" => 'required',
            "awb_number" => 'required',
            "weight" => 'required|numeric',
            "service_type" => 'required|in:regular,one_day',
            "is_insurance" => 'required|in:0,1',
            "parcel_price" => 'nullable|numeric'
        ];
        // make validation
        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $message = implode(' ',$errors);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($isInsurance){
            if (empty($parcelPrice)){
                $message = 'Parcel Price is Required';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }
        if (empty($parcelPrice)) $parcelPrice = 0;

        // get user db
        $userDb = User::where('phone',$username)->first();
        if (!$userDb){
            $message = "Invalid User";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerId = $userDb->locker_id;

        $lockerDb = Locker::find($lockerId);
        if (!$lockerDb){
            $message = 'Invalid Locker ID';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get virtual locker
        $virtualLockerDb = VirtualLocker::with('city')
            ->where('locker_id',$lockerId)
            ->first();
        if (!$virtualLockerDb){
            $message = 'Locker Not Found';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerCity = $virtualLockerDb->city->city_name;

        $cityOrigin = cache('express-cityOrigin',null);
        if (empty($cityOrigin)){
            // get from API
            $popExpressAPI = new ApiPopExpress();
            $getCity = $popExpressAPI->getCityOrigin();
            if (empty($getCity)){
                $message = 'Failed Get City';
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            if ($getCity->response->code !=200){
                $message = $getCity->response->message;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $cityOrigin = $getCity->data->origins;
            cache(['express-cityOrigin'=>$cityOrigin],Carbon::now()->addWeek(1));
        }
        if (empty($cityOrigin)){
            $message = 'Empty City Origin';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $isOrigin = null;
        $originCode = null;
        foreach ($cityOrigin as $item) {
            $tmpName = strtoupper($item->name);
            if (strpos($lockerCity,$tmpName) !== false) {
                $isOrigin = $item->name;
                $originCode = $item->code;
            }
        }
        if (empty($isOrigin)){
            $message = 'Out of Coverage';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get recipient address detail
        $deliveryDistrictDb = DeliveryDistrict::where('code',$recipientDistrictCode)->first();
        if (!$deliveryDistrictDb){
            $message = "Invalid District Code";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // create parameter for PopExpress
        $lockerName = $virtualLockerDb->locker_name;
        $pickupDate = date('Y-m-d');
        $pickupTime = "17.00 to 21.00";
        $pickupLocation = $virtualLockerDb->address;
        $branchId = $lockerDb->express_branch_id;
        if (empty($branchId)){
            $message = "Agent Not Activated for PopExpress. Invalid Branch ID";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $data = [];
        $data['order_no'] = $awbNumber;
        $data['sender_telp'] = $senderPhone;
        $data['sender_name'] = $senderName;
        $data['sender_email'] = $senderEmail;
        $data['notes'] = $notes;
        $data['weight'] = $weight;
        $data['service_type'] = $serviceType;
        $data['recipient_address'] = $recipientAddress;
        $data['recipient_telp'] = $recipientPhone;
        $data['recipient_name'] = $recipientName;
        $data['recipient_email'] = $recipientEmail;
        $data['origin'] = $isOrigin;
        $data['origin_code'] = $originCode;
        $data['destination_province'] = $deliveryDistrictDb->city->province->province_name;
        $data['destination_county'] = $deliveryDistrictDb->city->city_name;
        $data['destination_district'] = $deliveryDistrictDb->district_name;
        $data['destination_zipcode'] = '';
        $data['parcel_price'] = $parcelPrice;
        $data['insurance'] = $isInsurance;
        $data['reseller'] = $reseller;

        $calculate = $this->calculate($lockerName,$pickupDate,$pickupTime,$pickupLocation,$data);
        if (!$calculate->isSuccess){
            $message = $calculate->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $calculateResult = $calculate->data;
        $totalPrice = (int)$calculateResult->total_price;

        DB::beginTransaction();
        $username = $request->input('username');
        $userDb = User::where('username',$username)->first();
        $lockerId = $userDb->locker_id;

        $senderName = $request->input('sender_name');
        $itemName = "Kirim Barang $senderName";
        $itemType = 'delivery';
        $itemPrice = $totalPrice;
        $itemParam = $input;
        $itemPicture = null;

        unset($itemParam['agent_password']);
        unset($itemParam['username']);

        // create transaction
        $transactionDb = Transaction::createSingleTransaction($username,$itemName,$itemType,$itemPrice,$itemParam,$itemPicture);
        if (!$transactionDb->isSuccess){
            DB::rollback();
            $message = $transactionDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $transactionRef = $transactionDb->transactionRef;
        $transactionId = $transactionDb->transactionId;

        // update transaction items
        $transactionItems = TransactionItem::where('transaction_id',$transactionId)->first();
        if ($transactionItems){
            $transactionItemDb = TransactionItem::find($transactionItems->id);
            $transactionItemDb->reference = $awbNumber;
            $transactionItemDb->save();
        }

        // add deposit payment method
        $method = 'deposit';
        $paymentDb = Payment::addPayment($transactionRef,$username,$method);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // paid payment
        $paymentDb = Payment::paidPayment($transactionRef,$username,$agentPassword);
        if (!$paymentDb->isSuccess){
            DB::rollback();
            $message = $paymentDb->errorMsg;
            $resp=['response' => ['code' => '400','message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // commission calculate and transaction
        $commissionSchema = Payment::createCommissionTransaction($transactionRef);
        if (!$commissionSchema->isSuccess){
            DB::rollback();
            $message = $commissionSchema->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // push pop express
        $createOrderExpress = $this->submit($branchId,$lockerName,$pickupDate,$pickupTime,$pickupLocation,$data);
        if (!$createOrderExpress->isSuccess){
            $message = $createOrderExpress->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $createResult = $createOrderExpress->data;

        // save to Delivery Order
        $deliveryOrder = new DeliveryOrder();
        $createDelivery = $deliveryOrder->createOrder($lockerId,$pickupDate,$pickupTime,$pickupLocation,$awbNumber,$recipientDistrictCode,$weight,$data,$createResult);
        if (!$createDelivery){
            $message = $createDelivery->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // push to locker
        /*$params = [];
        $params['username'] = "agent_".$username;
        $params['locker_id'] = $lockerId;
        $params['service'] = "delivery";
        $params['order_id'] = $awbNumber;
        $params['phone'] = $senderPhone;
        $params['email'] = null;
        $params['description'] = $notes;
        $params['destination_name'] = $recipientName;
        $params['destination_phone'] = $recipientPhone;
        $params['status'] = 'CUSTOMER_STORE';

        $apiVirtual = new ApiVirtualLocker();
        $createVirtual = $apiVirtual->createParcel($params);
        if (empty($createVirtual)){
            $message = 'Failed Create Virtual';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        if ($createVirtual->response->code!=200){
            $message = $createVirtual->response->message;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }*/

        // update status
        $updateStatus = $deliveryOrder->updateStatus($awbNumber,'CUSTOMER_STORE');
        $totalPrice = (int)$createResult->total_price;
        // Calculate Schema
        $basicPrice = $totalPrice;
        $publishPrice = $totalPrice;
        $params = [];
        $params['transaction_type'] = 'delivery';
        $params['transaction_item_type'] = 'delivery';
        $params['user_id'] = $userDb->id;
        $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);

        DB::commit();

        $smsNotification = new SmsNotification();
        // send SMS for Recipient
        if (!empty($recipientPhone)){
            if (empty($recipientName)) $recipientName = $recipientPhone;
            if (empty($recipientName)) $recipientName = $recipientEmail;

            $recipientSms = "Hi, $recipientName. Paket Anda dari $senderName telah dikirimkan pada $pickupDate dg No Resi $awbNumber. www.popexpress.id";
            $createSms = $smsNotification->createSMS('Delivery',"global",$recipientPhone,$recipientSms);
        }

        // send SMS for sender
        if (!empty($senderPhone)){
            if (empty($senderName)) $senderName = $senderPhone;
            if (empty($senderName)) $senderName = $senderEmail;
            $tmpTotal = number_format($totalPrice);
            $senderSms = "Hi, $senderName. Paket Anda untuk $recipientName sedang di proses dg No Resi $awbNumber sejumlah Rp$tmpTotal. www.popexpress.id";
            $createSms = $smsNotification->createSMS("Delivery","global",$senderPhone,$senderSms);
        }

        $subTotalPrice = $createResult->weight * $createResult->price_per_kg;

        $response = new \stdClass();
        $response->awb_number = $awbNumber;
        $response->origin_city = $isOrigin;
        $response->destination_province = $data['destination_province'];
        $response->destination_city = $data['destination_county'];
        $response->destination_district = $data['destination_district'];
        $response->service_type = $createResult->service_type;
        $response->weight = $createResult->weight;
        $response->price_per_kg = $createResult->price_per_kg;
        $response->insurance_price = (int)$createResult->insurance_price;
        $response->sub_total_price = $subTotalPrice;
        $response->total_price = $totalPrice;
        $response->commission = $commission->commission;

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$response]];
        return response()->json($resp);
    }

    /**
     * Delivery Callback for PopExpress
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deliveryCallback(Request $request){
        // generate default response
        $response = new \stdClass();
        $response->code = 400;
        $response->message = null;

        $result = new \stdClass();
        $result->response = $response;
        $result->data = [];

        $input = $request->input();
        if (empty($request->input('data'))){
            $response->message = 'Empty Data';
            return response()->json($result);
        }
        // get data
        $expressData = $request->input('data');
        $expressData = $expressData[0];

        $awbNumber = $expressData['receipt_number'];
        $histories = $expressData['history']['status'];

        if (empty($histories)){
            $response->message = 'Empty history';
            return response()->json($result);
        }
        $lastHistory = end($histories);
        // find awb on DB
        $deliveryOrderDb = DeliveryOrder::where('awb_number',$awbNumber)->first();
        if (!$deliveryOrderDb){
            $response->message = 'AWB Number Not Found';
            return response()->json($result);
        }

        $status = 'ON_PROCESS';
        $remarks = $lastHistory['status'];

        if (strpos($remarks,'TELAH SELESAI') !== false){
            $status = 'COMPLETED';
        }

        DB::beginTransaction();

        $deliveryOrderDb = new DeliveryOrder();
        $updateStatus = $deliveryOrderDb->updateStatus($awbNumber,$status,$remarks);
        if (!$updateStatus->isSuccess){
            DB::rollback();
            $response->message = $updateStatus->errorMsg;
            return response()->json($result);
        }

        DB::commit();

        $response->code = 200;
        $response->message = 'Success Update Data';

        return response()->json($result);
    }

    /**
     * Get History Order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistories(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $username = $request->input('username');
        $userDb = User::where('phone',$username)->first();

        $lockerId = $userDb->locker_id;

        // generated param
        $params = $request->except('username');
        $params['locker_id'] = $lockerId;

        // get Orders
        $deliveryOrderDb = new DeliveryOrder();
        $listOrder = $deliveryOrderDb->getOrders($params);

        if (empty($listOrder)){
            $resp=['response' => ['code' => 400,'message' =>'Empty Order / Not Found'], 'data' =>$listOrder];
            return response()->json($resp);
        }


        $resp=['response' => ['code' => 200,'message' =>''], 'data' =>$listOrder];
        return response()->json($resp);
    }

    /*============================ Private Function ============================*/

    /**
     * Private Function Calculate
     * @param $lockerName
     * @param $pickupDate
     * @param $pickupTime
     * @param $pickupLocation
     * @param $data
     * @return \stdClass
     */
    private function calculate($lockerName,$pickupDate,$pickupTime,$pickupLocation,$data){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // push to popexpress
        $apiPopExpress = new ApiPopExpress();
        $calculate = $apiPopExpress->calculatePickup($lockerName,$pickupDate,$pickupTime,$pickupLocation,$data);
        if (empty($calculate)){
            $message = "Failed Calculate";
            $response->errorMsg = $message;
            return $response;
        }
        if (!$calculate->isSuccess){
            $message = $calculate->errorMsg;
            $response->errorMsg = $message;
            return $response;
        }
        $result = $calculate->data;
        if (empty($result)){
            $message = "Failed Calculate";
            $response->errorMsg = $message;
            return $response;
        }
        if ($result->response->code!= 200){
            $message = $result->response->message;
            $response->errorMsg = $message;
            return $response;
        }
        $resultData = $result->data;

        $response->isSuccess = true;
        $response->data = $resultData;
        return $response;
    }

    /**
     * Private Function Submit
     * @param $branchId
     * @param $lockerName
     * @param $pickupDate
     * @param $pickupTime
     * @param $pickupLocation
     * @param $data
     * @return \stdClass
     */
    private function submit($branchId,$lockerName,$pickupDate,$pickupTime,$pickupLocation,$data){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // push to popexpress
        $apiPopExpress = new ApiPopExpress();
        $calculate = $apiPopExpress->submitPickup($branchId,$lockerName,$pickupDate,$pickupTime,$pickupLocation,$data);
        if (empty($calculate)){
            $message = "Failed Submit";
            $response->errorMsg = $message;
            return $response;
        }
        if (!$calculate->isSuccess){
            $message = $calculate->errorMsg;
            $response->errorMsg = $message;
            return $response;
        }
        $result = $calculate->data;
        if (empty($result)){
            $message = "Failed Calculate";
            $response->errorMsg = $message;
            return $response;
        }
        if ($result->response->code!= 200){
            $message = $result->response->message;
            $response->errorMsg = $message;
            return $response;
        }
        $resultData = $result->data;

        $response->isSuccess = true;
        $response->data = $resultData;
        return $response;
    }
}
