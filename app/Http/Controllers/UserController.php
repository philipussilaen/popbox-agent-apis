<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Helper;
use App\Http\Helpers\WebCurl;
use App\Jobs\CronNewAgent;
use App\Models\Locker;
use App\Models\LockerVAOpen;
use App\Models\PasswordReset;
use App\Models\ReferralCampaign;
use App\Models\ReferralTransaction;
use App\Models\Transaction;
use App\Models\UserSession;
use App\Models\VirtualLocker;
use App\Models\VirtualLockerStatus;
use App\Models\VirtualLockerCity;
use App\Models\VirtualRegion;
use App\Models\VirtualLockerUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    var $curl;

    public function __construct() {
        $headers    = ['Content-Type: application/json'];
        $this->curl = new WebCurl($headers);
    }

    // function from old app
    protected $headers = array (
        'Content-Type: application/json'
    );
    protected $is_post = 0;

    /**
     * ========================= Base Function  ======================
     */

    private function post_data($url, $post_data = array(), $headers = array(), $options = array()) {
        $result = null;
        $curl = curl_init ();

        if ((is_array ( $options )) && count ( $options ) > 0) {
            $this->options = $options;
        }
        if ((is_array ( $headers )) && count ( $headers ) > 0) {
            $this->headers = $headers;
        }
        if ($this->is_post !== null) {
            $this->is_post = 1;
        }

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLOPT_POST, $this->is_post );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $post_data );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $this->headers );

        $content = curl_exec ( $curl );
        $response = curl_getinfo ( $curl );
        $result = json_decode ( $content, TRUE );

        curl_close ( $curl );
        return $result;
    }

    /**
     * ====================== End Base Function  ======================
     */

    /**
     * -------------- Public Function  --------------
     */

    /**
     * register agent
     * Mandatory phone, name, username, password, picture, id_number, id_type, id_picture
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    /*
     * ChangeLogs
     * 2017-05-17
     * #Remove email for required
     * #Email Activation Not Send
     */
    public function agentRegister(Request $request){
        //required param list
        $required = ['name','phone','password','id_number','id_type','id_picture','gcm_token',
            'locker_name','address','city_id','latitude','longitude','open_hour','close_hour','days','services'
        ];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // validate input
        // create rules
        $rules = [
            "name"  => 'required|regex:/^[\pL\s]+$/u',
            "email" => 'email|unique:users,email',
            "password" => 'required|min:6',
        ];
        // make validation
        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $message = implode(' ',$errors);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        DB::connection('popbox_virtual')->beginTransaction();
        DB::beginTransaction();

        $lockerName = $request->input('locker_name');
        $lockerName = preg_replace('/[^A-Za-z0-9]/', ' ',$lockerName);
        $input['locker_name'] = $lockerName;

        // checking referral
        $phone = $request->input('phone',null);
        $email = $request->input('email',null);
        $referralCode = $request->input('referral_code',null);
        $referralCampaignId = null;
        $userDb = User::where('phone',$phone)
                ->first();
        if ($userDb){
            $message = 'User Phone Already Exist';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if (!empty($referralCode)){
            $checkReferral = ReferralCampaign::checkingActiveCampaign($referralCode);
            if (!$checkReferral->isSuccess){
                $message = $checkReferral->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
            $referralCampaignId = $checkReferral->campaignId;

            // check usage
            $checkUsage = ReferralCampaign::checkUsage($referralCode);
            if (!$checkUsage->isSuccess){
                $message = $checkUsage->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }

            $limitUsage = $checkUsage->limitUsage;
            $usage = $checkUsage->usage;

            if (!empty($limitUsage)){
                if ($usage >= $limitUsage){
                    $message = 'Kode referral ini sudah mencapai maksimum pemakaian.';
                    $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                    return response()->json($resp);
                }
            }
        }


        // insert to DB
        $agentDb = User::insertAgentUser($input);
        if (!$agentDb->isSuccess){
            $message =  $agentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // insert user to popbox virtual
        $userLocker = VirtualLockerUser::insertAgentUser($input);
        if (!$userLocker->isSuccess){
            $message =  $userLocker->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $phone = $input['phone'];
        $agentUserName = "agent_$phone";

        // insert to virtual locker
        $lockerDb = VirtualLocker::insertLocker($input);
        if (!$lockerDb->isSuccess){
            $message =  $lockerDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $lockerId = $lockerDb->lockerId;
        $lockerName = strtoupper($input['locker_name']);

        // generate pin verification
        $pinVerification = Helper::generateRandomString(6);

        // insert new locker
        $lockerDb = new Locker();
        $lockerDb->id = $lockerId;
        $lockerDb->locker_name = $lockerName;
        $lockerDb->balance = 0;
        $lockerDb->pin_verification = $pinVerification;
        $lockerDb->id_verification = 'waiting|Menunggu Verifikasi';
        $lockerDb->status_verification = 'disable';
        $lockerDb->save();

        $param = [];
        $param['username'] = $input['phone'];
        $param['locker_id'] = $lockerId;
        $agentDb = User::updateAgentUser($param);
        if (!$agentDb->isSuccess){
            $message =  $agentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // insert new referral transaction
        if (!empty($referralCampaignId)){
            $referralTransaction = ReferralTransaction::createTransaction($referralCode,$referralCampaignId,$lockerId);
            if (!$referralTransaction->isSuccess){
                $message = $referralTransaction->errorMsg;
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }

        DB::connection('popbox_virtual')->commit();
        DB::commit();

        // create response
        $agentId = $agentDb->id;
        $agentDb = User::find($agentId);

        //

        // send email proccess
        /*$data = [];
        $data['name'] = $agentDb->name;
        $data['email'] = $agentDb->email;
        $url = url('auth/activation')."/$code";
        $data['url'] = $url;

        Mail::send('email.auth.activation', ['data' => $data], function ($m) use ($agentDb) {
            $m->from('no-reply@popbox.asia', 'PopBox Asia');
            $m->to($agentDb->email, $agentDb->name)->subject("[PopBox Agent] Aktivasi Akun :$agentDb->email");
        });*/



        $response = new \stdClass();
        $response->name = $agentDb->name;
        $response->phone = $agentDb->phone;
        $response->email = $agentDb->email;
        $response->username = $agentDb->username;
        $tmp[] = $response;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $tmp];
        return response()->json($resp);
    }

    /**
     * update agent
     * Mandatory : username
     * Optional : name, phone, email, password, picture, locker_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentUpdate(Request $request){
        //required param list
        $required = ['username'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // validate input
        // create rules
        $rules = [
            "phone" => 'numeric',
            "name"  => 'regex:/^[\pL\s\-]+$/u',
            "email" => 'email',
            "username" => 'required|alpha_dash|min:6',
            "password" => 'min:6',
            "id_type" => 'in:ktp,sim'
        ];
        // make validation
        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $message = implode(' ',$errors);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // update to DB
        $agentDb = User::updateAgentUser($input);
        if (!$agentDb->isSuccess){
            $message = $agentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // create response
        $agentId = $agentDb->id;
        $agentDb = User::find($agentId);

        $response = new \stdClass();
        $response->name = $agentDb->name;
        $response->phone = $agentDb->phone;
        $response->email = $agentDb->email;
        $response->username = $agentDb->username;
        $tmp[] = $response;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $tmp];
        return response()->json($resp);
    }

    /**
     * Activate Agent
     * Mandatory : code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentActivation(Request $request){
        //required param list
        $required = ['code'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $agentDb = User::ActivateAgentUser($input);
        if (!$agentDb->isSuccess){
            $message = $agentDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $resp=['response' => ['code' => 200,'message' =>''], 'data' => []];
        return response()->json($resp);
    }

    /**
     * login agent
     * Mandatory : user_input, password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentLogin(Request $request){
        //required param list
        $required = ['user_input','password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userInput = $request->json('user_input');
        $password = $request->json('password');
        $field = filter_var($userInput, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        // validate email or username
        $checkAgentUser = User::where($field,$userInput)->first();
        if (!$checkAgentUser){
            $message = "Invalid User Input";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if (!Hash::check($password,$checkAgentUser->password)){
            $message = "Invalid Credentials";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        if ($checkAgentUser->status==0){
            $message = "User Status not activated";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        if ($checkAgentUser->status==1){
            $message = "Waiting for Activation from PopBox";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        if ($checkAgentUser->status==3){
            $message = "Data Anda belum lengkap. Silahkan hubungi CS PopBox untuk lebih lanjut.";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        DB::beginTransaction();
        // create response
        $agentId = $checkAgentUser->id;
        $agentDb = User::find($agentId);

        $lockerType = DB::connection('popbox_virtual')
            ->table('lockers')
            ->where('locker_id', '=', $agentDb->locker_id)
            ->first();

        $response = new \stdClass();
		$response->user_id = $agentDb->id;
        $response->name = $agentDb->name;
        $response->phone = $agentDb->phone;
        $response->email = $agentDb->email;
        $response->username = $agentDb->username;
        $response->locker_id = $agentDb->locker_id;
        $response->status = $agentDb->status;
        if (!empty($agentDb->locker_id)){
            $response->balance = $agentDb->locker->balance;
        } else {
            $response->balance = null;
        }
        $response->is_near = false;
        $response->distance = null;
        $response->type = 'warung';//$lockerType->type;
        
		$response->region_code = '';
        // if locker id exist
        if (!empty($agentDb->locker_id)){
            $lockerId = $agentDb->locker_id;
            // get virtual locker db
            $virtualLockerDb = VirtualLocker::where('locker_id',$lockerId)->first();
            if (!empty($virtualLockerDb)){
                // calculate distance
                $currentLat = $request->input('latitude',null);
                $currentLong = $request->input('longitude',null);
                if (!empty($currentLat) && !empty($currentLong)){
                    $lockerLat = $virtualLockerDb->latitude;
                    $lockerLong = $virtualLockerDb->longitude;

                    $distance = Helper::haversineGreatCircleDistance($currentLat,$currentLong,$lockerLat,$lockerLong);
                    // if device distance is less than 1000m (1km) from registered locker location set is_near true, set locker status online
                    $response->distance = $distance;
                    if ($distance<1000){
                        // set is near
                        $response->is_near = true;
                        // update locker to online
                        VirtualLockerStatus::changeStatus($agentDb->locker_id,'ONLINE', $agentDb->id,$currentLat,$currentLong);
                        VirtualLocker::where('locker_id',$agentDb->locker_id)->update(['status'=>1,'last_ping'=>date('Y-m-d H:i:s')]);
                    }
                }
            }
			
            $regionDB = VirtualLockerCity::leftJoin('regions', 'cities.regions_id', '=', 'regions.id')
                ->select('regions.region_code')
                ->where('cities.id', '=', $lockerType->cities_id)
                ->first();

            if( !empty($regionDB->region_code)) {
                $response->region_code = $regionDB->region_code;
            }			
        }

        // create user session
        $userSession = UserSession::createUserSession($agentDb->id);
        if (!$userSession->isSuccess){
            $message = $userSession->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $sessionId = $userSession->sessionId;
        $response->session_id = $sessionId;

        $tmp[] = $response;
        DB::commit();
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => $tmp];
        return response()->json($resp);
    }

    /**
     * agent logout
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentLogout(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $username = $request->input('username');
        // validate email or username
        $checkAgentUser = User::where('username',$username)->first();
        if (!$checkAgentUser){
            $message = "Invalid User Input";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // change locker status to offline
        VirtualLockerStatus::changeStatus($checkAgentUser->locker_id,'OFFLINE');
        $resp=['response' => ['code' => 200,'message' =>null], 'data' => []];
        return response()->json($resp);
    }

    /**
     * check password
     * Mandatory : username and password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPassword(Request $request){
        //required param list
        $required = ['user_input','password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userInput = $request->json('user_input');
        $password = empty($input['password']) ? null : $input['password'];
        $field = filter_var($userInput, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        if( empty($password)) {
            $message = "Invalid Password";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // validate email or username
        $checkAgentUser = User::where($field,$userInput)->first();
        if (!$checkAgentUser){
            $message = "Invalid User";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $agentId = $checkAgentUser->id;
        $agentDb = User::find($agentId);

        if (!Hash::check($password,$agentDb->password)){
            $message = "Invalid Password";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => []];
        return response()->json($resp);
    }

    /**
     * checking agent user
     * Mandatory : user_input (email or phone number)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentCheck(Request $request){
        //required param list
        $required = ['user_input','password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userInput = $request->json('user_input');
        $field = filter_var($userInput, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        // validate email or username
        $checkAgentUser = User::where($field,$userInput)->first();
        if (!$checkAgentUser){
            $message = "Invalid User";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $agentId = $checkAgentUser->id;
        $agentDb = User::find($agentId);

        $password = empty($input['password']) ? null : $input['password'];
        if (!empty($password)){
            // checking password
            if (!Hash::check($password,$agentDb->password)){
                $message = "Invalid Password";
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }

        $response = new \stdClass();
        $response->name = $agentDb->name;
        $response->phone = $agentDb->phone;
        $response->email = $agentDb->email;
        $response->username = $agentDb->username;
        $response->locker_id = $agentDb->locker_id;
        $response->status = $agentDb->status;
        $response->id_number = $agentDb->id_number;
        $response->balance = 0;


        // get virtual locker data
        if (!empty($agentDb->locker_id)){
            $response->balance = $agentDb->locker->balance;
            $lockerDb = VirtualLocker::where('locker_id',$agentDb->locker_id)->first();
            $response->locker_name = null;
            $response->locker_address = null;
            if ($lockerDb){
                $response->locker_name = $lockerDb->locker_name;
                $response->locker_address = $lockerDb->address;
                $response->locker_address_detail = $lockerDb->detail_address;
                $services = [];
                foreach ($lockerDb->services as $item){
                    $services[] = $item->name;
                }
                $response->services = $services;
                $days = [];
                $openHour = [];
                $closeHour = [];
                foreach ($lockerDb->operational_time as $key => $time){
                    $days[$key] = $time->day;
                    $openHour[$key] = empty($time->open_hour) ? null : sprintf("%02d",$time->open_hour);
                    $closeHour[$key] = empty($time->close_hour) ? null : sprintf("%02d",$time->close_hour);
                }
                $response->days = $days;
                $response->open_hour = $openHour;
                $response->close_hour = $closeHour;
            }
        }

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$response]];
        return response()->json($resp);

    }

    /**
     * Get Agent Profile Data
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAgentProfile(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $username = $request->input('username');
        $agentDb = User::with('locker')
            ->where('username',$username)
            ->first();

        $response = new \stdClass();
        $response->name = $agentDb->name;
        $response->phone = $agentDb->phone;
        $response->email = $agentDb->email;
        $response->username = $agentDb->username;
        $response->locker_id = $agentDb->locker_id;
        $response->status = $agentDb->status;
        $response->id_number = $agentDb->id_number;
        $response->balance = null;
        $response->gcm_token = $agentDb->gcm_token;
        $response->status_verification = 'enable';

        $statusMessage = "Data Akun Anda Belum Lengkap";
        $statusVerification = $agentDb->locker->status_verification;
        if (empty($statusVerification)) $statusVerification = 'enable';
        $lockerData = $agentDb->locker;
        if ($statusVerification == 'pending') $statusMessage = "Akun Sedang Dalam Proses Verifikasi";
        if ($statusVerification == 'verified') $statusMessage = 'Profil Anda telah diverifikasi.';
        if ($statusVerification == 'disable')  $statusMessage = 'Akun Anda Sedang Dalam Pengawasan, Hubungi CS';
        $response->status_message = $statusMessage;
        // default value for all verification
        #id
        $idVerification = 'not_uploaded|Silahkan Upload Foto KTP';
        $idMessage = null;
        $idUrl = null;
        #selfie
        $selfieVerification = 'not_uploaded|Silahkan Upload foto selfie dengan KTP (KTP diposisikan di bawah dagu)';
        $selfieMessage = null;
        $selfieUrl = null;
        #store
        $storeVerification = 'not_uploaded|Silahkan Upload Foto Toko tampak depan';
        $storeMessage = null;
        $storeUrl = null;
        #npwp
        $npwpVerification = 'not_uploaded|Silahkan Upload Foto NPWP';
        $npwpMessage = null;
        $npwpUrl = null;

        // ID Verification
        if ($statusVerification == 'verified') {
            $idVerification = 'valid|Sukses Terverifikasi';
            if (!empty($agentDb->id_picture)) $idUrl = url("/agent/id/".$agentDb->id_picture);
        } else {
            if (!empty($lockerData->id_verification) && !empty($agentDb->id_picture)){
                $idVerification = $lockerData->id_verification;
                if (!empty($agentDb->id_picture)) $idUrl = url("/agent/id/".$agentDb->id_picture);
            }
        }
        list($idVerification,$idMessage) = array_pad( explode('|',$idVerification),2,null);

        // Selfie Verification
        if ($statusVerification == 'verified') {
            $selfieVerification = 'valid|Sukses Terverifikasi';
            if (!empty($agentDb->id_selfie)) $selfieUrl = url("/agent/id_selfie/".$agentDb->id_selfie);
        } else {
            if (!empty($lockerData->selfie_verification) && !empty($agentDb->id_selfie)){
                $selfieVerification = $lockerData->selfie_verification;
                if (!empty($agentDb->id_selfie)) $selfieUrl = url("/agent/id_selfie/".$agentDb->id_selfie);
            }
        }
        list($selfieVerification,$selfieMessage) = array_pad( explode('|',$selfieVerification),2,null);

        // Store Verification
        $lockerUrl = env('VL_URL');
        $lockerUrl = str_replace('api','',$lockerUrl);
        $filename = null;
        $virtualLockerDb = VirtualLocker::where('locker_id',$agentDb->locker_id)->first();
        if ($virtualLockerDb){
            $filename = $virtualLockerDb->picture;
        }

        if ($statusVerification == 'verified') {
            $storeVerification = 'valid|Sukses Terverifikasi';
            if (!empty($filename))  $storeUrl = $lockerUrl."/img/locker/".$filename;
        } else {
            if (!empty($lockerData->store_verification) && !empty($filename)){
                $storeVerification = $lockerData->store_verification;
                if (!empty($filename))  $storeUrl = $lockerUrl."/img/locker/".$filename;
            }
        }
        list($storeVerification,$storeMessage) = array_pad( explode('|',$storeVerification),2,null);


        // NPWP Verification
        if ($statusVerification == 'verified') {
            $npwpVerification = 'valid|Sukses Terverifikasi';
            if (!empty($agentDb->npwp_picture)) $npwpUrl = url("/agent/npwp/".$agentDb->npwp_picture);
        } else {
            if (!empty($lockerData->npwp_verification) && !empty($agentDb->npwp_picture)){
                $npwpVerification = $lockerData->npwp_verification;
                if (!empty($agentDb->npwp_picture)) $npwpUrl = url("/agent/npwp/".$agentDb->npwp_picture);
            }
        }
        list($npwpVerification,$npwpMessage) = array_pad( explode('|',$npwpVerification),2,null);

        $response->status_verification = $statusVerification;

        $stepVerification = new \stdClass();
        $stepVerification->id_verification = $idVerification;
        $stepVerification->id_message = $idMessage;
        $stepVerification->id_url = $idUrl;

        $stepVerification->selfie_verification = $selfieVerification;
        $stepVerification->selfie_message = $selfieMessage;
        $stepVerification->selfie_url = $selfieUrl;

        $stepVerification->store_verification = $storeVerification;
        $stepVerification->store_message = $storeMessage;
        $stepVerification->store_url = $storeUrl;

        $stepVerification->npwp_verification = $npwpVerification;
        $stepVerification->npwp_message = $npwpMessage;
        $stepVerification->npwp_url = $npwpUrl;

        $response->data_verification[] = $stepVerification;

        // get transaction
        $param = [];
        $param['type'] = ['purchase','payment'];
        $param['status'] = 'PAID';
       	$param['start_date'] = date('Y-m-1')." 00:00:00";
        $param['end_date'] = date('Y-m-t'). "23:59:59";
        $transactionDb = Transaction::listAllTransaction($username,$param);
        $transactionList = collect($transactionDb->data);
        $totalTransaction = $transactionList->count();
        $amountTransaction = $transactionList->sum('total_price');

        // get commission
        $param = [];
        $param['type'] = 'commission';
        $param['status'] = 'PAID';
        $param['start_date'] = date('Y-m-1')." 00:00:00";
        $param['end_date'] = date('Y-m-t'). "23:59:59";
        $transactionDb = Transaction::listAllTransaction($username,$param);
        $transactionList = collect($transactionDb->data);
        $amountCommission = $transactionList->sum('total_price');

        $response->total_transaction = $totalTransaction;
        $response->amount_transaction = $amountTransaction;
        $response->amount_commission = $amountCommission;

        // get virtual locker data
        if (!empty($agentDb->locker_id)){
            $response->balance = $agentDb->locker->balance;
            $lockerDb = VirtualLocker::where('locker_id',$agentDb->locker_id)->first();
            $response->locker_name = null;
            $response->locker_address = null;
            if ($lockerDb){
                $response->locker_name = $lockerDb->locker_name;
                $response->locker_address = $lockerDb->address;
                $response->locker_address_detail = $lockerDb->detail_address;
                $services = [];
                foreach ($lockerDb->services as $item){
                    $services[] = $item->code;
                }
                $response->services = $services;
                $days = [];
                $openHour = [];
                $closeHour = [];
                foreach ($lockerDb->operational_time as $key => $time){
                    $days[$key] = $time->day;
                    $openHour[$key] = empty($time->open_hour) ? null : sprintf("%02d",$time->open_hour);
                    $closeHour[$key] = empty($time->close_hour) ? null : sprintf("%02d",$time->close_hour);
                }
                $response->days = $days;
                $response->open_hour = $openHour;
                $response->close_hour = $closeHour;
            }
        }

        // get agent va open
        $lockerVAOpen = LockerVAOpen::where('lockers_id',$agentDb->locker_id)->get();
        $listAvailableVA = [];
        foreach ($lockerVAOpen as $item) {
            $tmp = new \stdClass();
            $tmp->va_type = $item->va_type;
            $tmp->va_number = $item->va_number;
            $tmp->time_limit = $item->expired_date;
            $listAvailableVA[] = $tmp;
        }
        $response->list_va_topup = $listAvailableVA;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$response]];
        return response()->json($resp);
    }

    /**
     * send link for reset password request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(Request $request){
        //required param list
        $required = ['user_input'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userInput = $request->json('user_input');
        $field = filter_var($userInput, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        // validate email or username
        $checkAgentUser = User::where($field,$userInput)->first();
        if (!$checkAgentUser){
            $message = "Akun tidak terdaftar";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // insert into DB
        $resetDb = PasswordReset::insertNew($checkAgentUser);
        if ($resetDb->isSuccess==false){
            $message = 'Failed to Create Forgot Password';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // get code
        $code = $resetDb->code;
        $url = url('auth/reset')."/$code";
        // create data to email view
        $data = [];
        $data['name'] = $checkAgentUser->name;
        $data['email'] = $checkAgentUser->email;
        $data['url'] = $url;

        Mail::send('email.auth.forgot', ['data' => $data], function ($m) use ($checkAgentUser) {
            $m->from('no-reply@popbox.asia', 'PopBox Asia');
            $m->to($checkAgentUser->email, $checkAgentUser->name)->subject("[PopBox Agent] Reset Kata Sandi :$checkAgentUser->email");
        });

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => []];
        return response()->json($resp);
    }

    /**
     * reset password user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(Request $request){
        //required param list
        $required = ['code','password'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $code = $request->input('code');
        $password = $request->input('password');

        // reset to DB
        $result = PasswordReset::resetPassword($code,$password);
        if ($result->isSuccess==false){
            $resp=['response' => ['code' => 400,'message' =>$result->errorMsg], 'data' => []];
            return response()->json($resp);
        }
        $resp=['response' => ['code' => 200,'message' =>''], 'data' => []];
        return response()->json($resp);
    }

    /**
     * ping function for locker status and balance
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserPing(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // get username
        $username = $input['username'];

        // get user detail
        $userDb = User::where('username',$username)->first();
        if (!$userDb) {
            $message = "Agent Not Found";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerId = $userDb->locker_id;
        // create default response
        $response = new \stdClass();
        $response->agent_name = $userDb->name;
        $response->locker_id = $userDb->locker_id;
        $response->locker_name = null;
        $response->is_near = false;
        $response->balance = 0;
        $response->distance = 0;

        // get lat long
        $latitude = $request->input('latitude',null);
        $longitude = $request->input('longitude',null);
        VirtualLockerStatus::changeStatus($lockerId,'ONLINE', $userDb->id,$latitude,$longitude);
        /*if (!empty($latitude) && !empty($longitude)){
            DB::beginTransaction();
            if (!empty($userDb->locker_id)){
                // get lockerDb
                $virtualLockerDb = VirtualLocker::where('locker_id',$lockerId)->first();
                if (!empty($virtualLockerDb)){
                    // get locker lat long and name
                    $lockerLatitude = $virtualLockerDb->latitude;
                    $lockerLongitude = $virtualLockerDb->longitude;
                    $lockerName = $virtualLockerDb->locker_name;

                    $distance = Helper::haversineGreatCircleDistance($latitude,$longitude,$lockerLatitude,$lockerLongitude);
                    // if device distance is less than 1000m (1km) from registered locker location set is_near true, set locker status online
                    $response->distance = $distance;
                    $response->locker_name = $lockerName;
                    $date = date('Y-m-d H:i:s');
                    if ($distance<1000){
                        // set is near
                        $response->is_near = true;
                        // update locker to online
                        VirtualLockerStatus::changeStatus($lockerId,'ONLINE', $userDb->id,$latitude,$longitude);
                    } else {
                        VirtualLockerStatus::changeStatus($lockerId,'OUTRANGE', $userDb->id,$latitude,$longitude);
                    }
                }
            }
            DB::commit();
        }*/

        // get locker balance
        $lockerDb = Locker::where('id',$lockerId)->first();
        if ($lockerDb){
            $response->balance = $lockerDb->balance;
        }

        $resp=['response' => ['code' => 200,'message' =>'OK'], 'data' => [$response]];
        return response()->json($resp);
    }

    /**
     * Change Password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        //required param list
        $required = ['old_password','new_password','new_password_confirmation'];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $oldPassword = $request->input('old_password');
        $newPassword = $request->input('new_password');
        $newConfirmation = $request->input('new_password_confirmation');

        // get user DB
        $username = $request->input('username');
        $userDb = User::where('username',$username)->first();
        // check old password
        if (!Hash::check($oldPassword,$userDb->password)){
            $message = 'Kata sandi yang lama tidak sesuai';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // validate password and confirmation
        if ($newPassword!==$newConfirmation){
            $message = 'Password Not Match';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // check length
        if (strlen($newPassword)<6){
            $message = 'Password Length Minimum 6 Character';
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // validate password
        $rules= [
            'new_password' => 'required|alpha_dash'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()){
            $errors = $validator->errors();
            $message = $errors->first();
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // reset password
        $userDb = User::find($userDb->id);
        $userDb->password = Hash::make($newPassword);
        $userDb->save();

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => []];
        return response()->json($resp);
    }

    /**
     * Web Activation
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function activationAgent(Request $request, $id=null){
        if (empty($id)) return "Invalid Id";
        $input = [];
        $input['code'] = $id;
        $agentDb = User::ActivateAgentUser($input);
        if (!$agentDb->isSuccess){
            $message = $agentDb->errorMsg;
            return $message;
        }
        return "Success Activation. Please Wait for PopBox Activation";
    }

    /**
     * check / update token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkGcmToken(Request $request){
        //required param list
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $gcmToken = $request->input('gcm_token',null);
        $username = $request->input('username');

        $userDb = User::where('username',$username)->first();
        $currentGcmToken = null;

        if (empty($gcmToken)){
            $currentGcmToken = $userDb->gcm_token;
            if (empty($currentGcmToken)) {
                $message = "GCM token not exist on user $username";
                $resp = ['response' => ['code' => 400, 'message' => $message], 'data' => []];
                return response()->json($resp);
            }
        } else {
            $currentGcmToken = $userDb->gcm_token;
            if (empty($currentGcmToken)){
                // insert gcm token
                $userDb = User::find($userDb->id);
                $userDb->gcm_token = $gcmToken;
                $userDb->save();
                $currentGcmToken = $gcmToken;
            } else {
                // check if current gcm token same with sent gcm token
                if ($currentGcmToken != $gcmToken){
                    $userDb = User::find($userDb->id);
                    $userDb->gcm_token = $gcmToken;
                    $userDb->save();
                    $currentGcmToken = $gcmToken;
                }
            }
        }

        // create response
        $response = new \stdClass();
        $response->username = $username;
        $response->gcm_token = $currentGcmToken;

        $resp=['response' => ['code' => 200,'message' =>null], 'data' => [$response]];
        return response()->json($resp);
    }

    /**
     * get reset password page
     * @param null $code
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getResetPage($code=null,Request $request){
        if (empty($code)){
            return "Invalid Code";
        }
        // parsing data to view
        $data = [];
        $data['code'] = $code;
        return view('reset',$data);
    }

    /**
     * post reset page
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postResetPassword(Request $request){
        // create validation
        $rules = [
            'code' => 'required',
            'password' => 'required|min:6',
            'repassword' => 'required|min:6|same:password'
        ];
        $this->validate($request,$rules);

        // create param
        $code = $request->input('code');
        $password = $request->input('password');
        // reset to DB
        $result = PasswordReset::resetPassword($code,$password);
        if ($result->isSuccess==false){
            $request->session()->flash('error',$result->errorMsg);
            return back();
        }
        $request->session()->flash('success','Sukses reset kata sandi. Silahkan login / masuk menggunakan kata sandi baru.');
        return back();
    }

    /**
     * get agreement page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAgreementPage(Request $request){
        return view('email.auth.agreement');
    }

    /**
     * Get Referral For Profile User Page
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReferralProfile(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $userPhone = $request->input('username');
        // get UserDB
        $userDb = User::where('phone',$userPhone)->first();
        if (!$userDb){
            $message = "Invalid User";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $lockerId = $userDb->locker_id;
        if (empty($lockerId)){
            $message = "Invalid Locker Id";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        // get locker
        $lockerDb = Locker::find($lockerId);
        if (empty($lockerDb)){
            $message = "Invalid Locker";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        // create response
        $response = new \stdClass();
        $response->referral_code = null;
        $response->description = null;
        $response->description_to_shared = null;
        $response->limit_usage = 0;
        $response->usage = 0;

        if (!empty($lockerDb->referral_code)){
            $response->referral_code = $lockerDb->referral_code;
        }
        // get active referral
        $referralDb = ReferralCampaign::getActiveReferralCampaign();
        if ($referralDb->isSuccess){
            $campaignId = $referralDb->campaignId;
            $referralDb = ReferralCampaign::find($campaignId);
            $response->description = $referralDb->description;
            $response->description_to_shared = "Kode Referral: $response->referral_code. ".$referralDb->description_to_shared;
        }

        // get usage
        $referralDb = ReferralCampaign::checkUsage($response->referral_code);
        if ($referralDb->isSuccess){
            $response->limit_usage = $referralDb->limitUsage;
            $response->usage = $referralDb->usage;

            if (!empty($response->limit_usage)){
                $message = 'Bonus berlaku untuk '.$referralDb->limitUsage.' teman baru, Anda telah mengajak '.$referralDb->usage.' teman baru.';
                $response->description .= $message;
            }
        }

        $resp=['response' => ['code' => 200,'message' =>''], 'data' => [$response]];
        return response()->json($resp);
    }

    /**
     * Post Complete Profile
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postCompleteProfile(Request $request){
        $required = [];
        // get all param
        $input = $request->except('token');
        $paramFailed = array();
        foreach ($required as $item) {
            if (!array_key_exists($item,$input)) $paramFailed[] = $item;
        }
        // if there is missing parameter
        if (!empty($paramFailed)){
            $message = "Missing Parameter : ".implode(', ',$paramFailed);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $sessionId = $request->input('session_id');
        // get userDB
        $userSessionDb = UserSession::where('session_id',$sessionId)->first();
        if (!$userSessionDb){
            $message = "Invalid User Session";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userId = $userSessionDb->user_id;

        // get locker database
        $userDb = User::find($userId);
        $lockerDb = Locker::find($userDb->locker_id);
        // ID Verification
        $idVerification = 'not_uploaded';
        if (!empty($lockerDb->id_verification)){
            $idVerification = $lockerDb->id_verification;
            list($idVerification,$idMessage) = array_pad( explode('|',$idVerification),2,null);
        }

        // Selfie Verification
        $selfieVerification = 'not_uploaded';
        if (!empty($lockerDb->selfie_verification)){
            $selfieVerification = $lockerDb->selfie_verification;
            list($selfieVerification,$selfieMessage) = array_pad( explode('|',$selfieVerification),2,null);
        }

        // Store Verification
        $storeVerification = 'not_uploaded';
        if (!empty($lockerDb->store_verification)){
            $storeVerification = $lockerDb->store_verification;
            list($storeVerification,$storeMessage) = array_pad( explode('|',$storeVerification),2,null);
        }

        // NPWP Verification
        $npwpVerification = 'not_uploaded';
        if (!empty($lockerDb->npwp_verification)){
            $npwpVerification = $lockerDb->npwp_verification;
            list($npwpVerification,$npwpMessage) = array_pad( explode('|',$npwpVerification),2,null);
        }


        $rules = [];
        $notRequiredState = ['valid','waiting'];
        if (!in_array($idVerification,$notRequiredState)) $rules['id_picture'] = 'required';
        if (!in_array($selfieVerification,$notRequiredState)) $rules['id_selfie_picture'] = 'required';
        if (!in_array($storeVerification,$notRequiredState)) $rules['store_photo'] = 'required';
        // if (!in_array($npwpVerification,$notRequiredState)) $rules['npwp_picture'] = 'required';

        $validator = Validator::make($input,$rules);
        if ($validator->fails()){
            $errors = $validator->errors()->all();
            $message = implode(' ',$errors);
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }

        $idPhoto = $request->input('id_picture',null);
        $storePhoto = $request->input('store_photo',null);
        $idSelfiePicture = $request->input('id_selfie_picture',null);
        $npwpPicture = $request->input('npwp_picture',null);
        $pin = $request->input('pin',null);

        // get userDB
        $userSessionDb = UserSession::where('session_id',$sessionId)->first();
        if (!$userSessionDb){
            $message = "Invalid User Session";
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }
        $userId = $userSessionDb->user_id;

        DB::beginTransaction();
        DB::connection('popbox_virtual')->beginTransaction();

        // update to user DB
        $userDb = User::find($userId);
        $username = $userDb->username;

        $agentLockerDb = Locker::find($userDb->locker_id);
        if (!empty($pin) && !empty($agentLockerDb->pin_verification)){
            if ($pin != $agentLockerDb->pin_verification){
                $message = "Invalid PIN";
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }

        $agentLockerDb->status_verification = 'pending';

        if ($idPhoto){
            $filename = $username.'-'.date('Ymd').'.jpg';
            $path = public_path()."/agent/id/$filename";
            $img = base64_decode($idPhoto);
            $saving = file_put_contents($path,$img);
            $agentLockerDb->id_verification = 'waiting|Menunggu Verifikasi';
            $userDb->id_picture = $filename;
            if (!$saving){
                DB::rollback();
                DB::connection('popbox_virtual')->rollback();
                $message = "Failed to Save Id";
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }

        if ($idSelfiePicture){
            $filename = $username.'-'.date('Ymd').'.jpg';
            $path = public_path()."/agent/id_selfie/$filename";
            $img = base64_decode($idSelfiePicture);
            $saving = file_put_contents($path,$img);
            $agentLockerDb->selfie_verification = 'waiting|Menunggu Verifikasi';
            $userDb->id_selfie = $filename;
            if (!$saving){
                DB::rollback();
                DB::connection('popbox_virtual')->rollback();
                $message = "Failed to Save Id";
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }

        if ($npwpPicture){
            $filename = $username.'-'.date('Ymd').'.jpg';
            $path =public_path()."/agent/npwp/$filename";
            $img = base64_decode($npwpPicture);
            $saving = file_put_contents($path,$img);
            $userDb->npwp_picture = $filename;
            $agentLockerDb->npwp_verification = 'waiting|Menunggu Verifikasi';
            if (!$saving){
                DB::rollback();
                DB::connection('popbox_virtual')->rollback();
                $message = "Failed to Save Id";
                $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
                return response()->json($resp);
            }
        }


        $lockerDb = VirtualLocker::where('locker_id',$userDb->locker_id)->first();
        $lockerDb = VirtualLocker::find($lockerDb->id);

        $lockerId = $lockerDb->locker_id;

        if ($storePhoto){
            $filename = $lockerId.'.jpg';
//             $server =  $_SERVER['DOCUMENT_ROOT'];
//             $path = $server.env('LOCKER_LOC')."/img/locker/$filename";
//             $img = base64_decode($storePhoto);
//             $saving = file_put_contents($path,$img);
//             if (!$saving){
//                 $message = "Failed to Save Picture";
//                 $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
//                 return response()->json($resp);
//             }
            Storage::disk('storePict')->put($filename, base64_decode($storePhoto));
            $agentLockerDb->store_verification = 'waiting|Menunggu Verifikasi';
            $lockerDb->picture = $filename;
        }

        $lockerDb->save();
        $agentLockerDb->save();
        $userDb->save();

        DB::commit();
        DB::connection('popbox_virtual')->commit();

        $data = [];
        $data['agentName'] = $agentLockerDb->locker_name;
        $data['lockerId'] = $agentLockerDb->id;

        Mail::send('email.auth.notif-complete', ['data' => $data], function ($m) use ($agentLockerDb) {
            $m->from('it@popbox.asia', 'IT Team');
            $m->to('bilkis@popbox.asia', 'Bilkis')->subject("[PopBox Agent] Agent Complete Profil $agentLockerDb->locker_name");
            $m->cc('agent.ops@popbox.asia', 'Dyah Ayu')->subject("[PopBox Agent] Agent Complete Profil $agentLockerDb->locker_name");
        });

        $resp=['response' => ['code' => 200,'message' =>'Sukses Pembaharuan Profil'], 'data' => []];
        return response()->json($resp);
    }

    /**
     * ------------- End Public Function -------------
     */
}
