<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerProvince extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'provinces';
}
