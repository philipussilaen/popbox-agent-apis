<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerStatusDetail extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'locker_status_details';

    /**
     * insert new status detail / history
     * @param $statusId Locker Status primary key (id)
     * @param string $status
     * @return \stdClass
     */
    public static function insertStatusDetail($statusId, $status='OFFLINE'){
        $response = new \stdClass();
        $response->isSuccess = false;

        // insert status detail
        $dataDb = new self();
        $dataDb->locker_status_id = $statusId;
        $dataDb->status = $status;
        if ($dataDb->save()){
            $response->isSuccess = true;
        }
        return $response;
    }
}
