<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Locker extends Model
{
    // set table
    protected $table = 'lockers';
    protected $primaryKey = 'id';
    public $incrementing = false;

    /**
     * @param $transactionDb
     * @return \stdClass
     */
    public static function processTopUp($transactionDb){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $lockerId = $transactionDb->user->locker_id;
        $amount = $transactionDb->total_price;
        $transactionId = $transactionDb->id;

        // process topup
        $payment = Payment::creditDeposit($lockerId,$amount,$transactionId);
        if (!$payment->isSuccess){
            $response->errorMsg = 'Failed to top Up';
            return $response;
        }
        $response->isSuccess = true;
        return $response;
    }

    public function lockerVAOpens(){
        return $this->hasMany(LockerVAOpen::class,'lockers_id','id');
    }

    public function users(){
        return $this->hasMany(User::class,'locker_id','id');
    }
}
