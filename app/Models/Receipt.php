<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use App\Models\ReceiptBatch;
use App\Models\ReceiptItem;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Receipt extends Model
{
    use SoftDeletes;
    // set table
    protected $table = 'receipt';
    // set date attributes
    protected $dates = ['created_at','updated_at','deleted_at'];    

    /**
     * get By ID Receipt
     * @param receipt
     * @return \stdClass
     */
    public static function getReceipt($transactionRef, $userID, $statusId, $startDate, $endDate) {
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        
        // get receipt DB
        $receiptDb = self::leftJoin('transactions', 'receipt.transaction_id', '=', 'transactions.id')
            ->leftJoin('payments', 'transactions.id', '=', 'payments.transactions_id')
            ->leftJoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
            ->where('transactions.users_id', $userID)
            ->orderBy('transactions.created_at', 'desc')
            ->select( 
                DB::raw("
                    receipt.id AS receiptid, transactions.id AS transactionid, date_format(transactions.updated_at, '%d/%m/%Y') AS transactiondate, 
                    transactions.reference, transactions.total_price, receipt.status_receipt as status_receipt_id,
                    case 
                        when receipt.status_receipt = 1 then 'Belum Dikonfirmasi'
                        when receipt.status_receipt = 2 then 'Terima Sebagian'
                        when receipt.status_receipt = 3 then 'Sudah Terima Semua Barang'
                        else null
                        end as status_receipt,                        
                    payment_methods.name AS payment_name
                ") 
            );
        
        if( !empty($transactionRef)) {
            $receiptDb = $receiptDb->where('transactions.reference', $transactionRef);
        }
        if( !empty($statusId)) {
            $receiptDb = $receiptDb->where('receipt.status_receipt', $statusId);
        }
        if( !empty($startDate) && !empty($endDate) ) {
            $receiptDb = $receiptDb->whereRaw("date(transactions.updated_at) between str_to_date(?,'%d/%m/%Y') and str_to_date(?,'%d/%m/%Y')", [$startDate, $endDate]);
        }

        if($receiptDb->count() == 0) {
            $response->isSuccess = false;
            $response->errorMsg = "Tidak ada transaksi pada rentang tanggal tersebut";
            return $response;
        }

        $receiptDb = $receiptDb->get();

        foreach($receiptDb as $item) {
            // create response data
            $data = new \stdClass();
            $data->receipt_id = $item['receiptid'];
            $data->transaction_date = $item['transactiondate'];
            $data->reference = $item['reference'];
            $data->total_price = $item['total_price'];
            $data->status_receipt_id = $item['status_receipt_id'];
            $data->status_receipt = $item['status_receipt'];
            $data->payment_name = $item['payment_name'];

            // Count Qty at detail transaction
            $transactionItemDb = TransactionItem::where('transaction_id', $item['transactionid'])->get();
            
            $totalQty = null;
            foreach($transactionItemDb as $r) {
                $params = json_decode($r->params);
                $totalQty += (int) $params->amount;
            }

            $data->total_qty = $totalQty;
            $data->receipt_batch = ReceiptBatch::getReceiptBatch($item['receiptid']);

            $response->data[] = $data;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * update Qty Receipt by Batch
     * @param receipt
     * @return \stdClass
     */
    public static function updQtyReceiptByBatch($transactionRef,$username,$batchNumber,$receiptItems) {
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;
        
        $userDb = User::where('username',$username)->first();

        $receiptDb = self::where('reference', $transactionRef)->first();
        $transactionID = $receiptDb->transaction_id;
        $receiptID = $receiptDb->id;

        $receiptBatchDb = ReceiptBatch::where('receipt_id', $receiptID)
            ->where('batch_no', $batchNumber)->first();
        $batchID = $receiptBatchDb->id;

        DB::beginTransaction();

        $o=0; $countGAP=0; $countQty=[]; 
        foreach($receiptItems as $r) {
            $temp = [];
            $temp['id'] = $r['item_id'];
            $temp['qty_receipt'] = $r['qty_receipt'];
            $temp['gap_qty'] = $r['gap_qty'];

            $receiptItemDb = ReceiptItem::updateBatchItem($temp);

            if( empty($r['qty_receipt'])) $countQty[] = $r['item_id'];

            if( !$receiptItemDb->isSuccess) $o++;

            if($receiptItemDb->isSuccess) {
                if($receiptItemDb->data['gap'] > 0) {
                    $countGAP++;
                }
            }
        }

        $statusBatch = 3;
        if(sizeof($countQty) != sizeof($receiptItems)) {
            if($countGAP > 0) {
                $statusBatch = 2;
            }    
        } 
        else {
            $statusBatch = 4;
        }

        $receiptBatchDb = ReceiptBatch::where('id', $batchID)
            ->update([
                'status_batch' => $statusBatch,
            ]);

        if ( !$receiptBatchDb){
            DB::rollback();
            $message = $transDb->errorMsg;
            $resp=['response' => ['code' => 400,'message' =>$message], 'data' => []];
            return response()->json($resp);
        }        
        DB::commit();

        if($o > 0) {
            $response->isSuccess = false;
            $response->errorMsg = 'Data has been failure';
        }
        else {
            $response->isSuccess = true;
            $response->errorMsg = '';
            $response->data = $receiptItems;
        }

        return $response;
    }

    /**
     * Suggest Delivery
     * @param 
     * @return \stdClass
     */
    public static function getReceiptSuggestDelivery($receiptid) {
        $receiptDb = DB::select("
            select a.id as receiptid, c.id as transaction_items_id, ifnull(cast(json_extract(c.params, '$.amount') as unsigned),0) as qty_order, ifnull(d.qty_delivery,0) as qty_delivery,
                (ifnull(cast(json_extract(c.params, '$.amount') as unsigned),0) - ifnull(d.qty_receipt,0)) as gap_qty,
                c.name, c.params
            from receipt a
            left join transactions b on a.transaction_id = b.id
            left join transaction_items c on b.id = c.transaction_id
            left join (
                select a.id as receiptid, c.transaction_items_id, ifnull(sum(c.qty_delivery),0) as qty_delivery, ifnull(sum(c.qty_receipt),0) as qty_receipt
                from receipt a
                left join receipt_batch b on b.receipt_id = a.id
                left join receipt_items c on c.receipt_batch_id = b.id
                group by a.id, c.transaction_items_id
            ) d on c.id = d.transaction_items_id
            where a.id = ?    
        ", [$receiptid]);
        return $receiptDb;
    }

    /**
     * Insert Receipt
     * @param 
     * @return \stdClass
     */
    public static function insert($params) {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $mresult = new self();
        $mresult->transaction_id = $params['transaction_id'];
        $mresult->locker_id = $params['locker_id'];
        $mresult->reference = $params['reference'];
        $mresult->status_receipt = $params['status_receipt'];
        $mresult->created_at = date('Y-m-d H:i:s');
        $mresult->updated_at = date('Y-m-d H:i:s');
        $mresult->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Update Status Receipt
     * @param 
     * @return \stdClass
     */
    public static function updateStatus($statusid, $receiptid) {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $receiptDb = Receipt::where('id', $receiptid)
            ->update([
                'status_receipt' => $statusid
            ]);

        if($receiptDb) {
            $response->isSuccess = true;
        }

        return $response;
    }

    /*Relationship*/
    public function itemBatch(){
        return $this->hasMany(ReceiptBatch::class,'receipt_id','id');
    }
}
