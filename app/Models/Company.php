<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    // set tables
    protected $table = 'companies';

    public static function apiCheckToken($token){
        $check = self::where('token',$token)->first();
        if (!$check) return false;
        return true;
    }
}
