<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VirtualLockerWebNotification extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'web_notifications';

    /**
     * @param string $module
     * @param string $destination
     * @param string $destId
     * @param string $type
     * @param string $content
     * @param null $url
     * @return \stdClass
     */
    public function createNotification($module="",$destination="lockers",$destId = "1",$type = "info",$content="", $url = null){
        // generate standard content
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // set variable
        $this->varModule = $module;
        $this->varDestination = $destination;
        $this->varDestId = $destId;
        $this->varType = $type;
        $this->varContent = $content;

        // insert to db
        DB::beginTransaction();

        $notifDb = new self();
        $notifDb->module = $this->varModule;
        $notifDb->destination = $this->varDestination;
        $notifDb->destination_id = $this->varDestId;
        $notifDb->type = $this->varType;
        $notifDb->content = $this->varContent;

        $notifDb->save();

        DB::commit();
        $response->isSuccess = true;
        return $response;
    }
}
