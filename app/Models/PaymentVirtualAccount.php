<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentVirtualAccount extends Model
{
    protected $table = 'payment_virtual_accounts';

    /**
     * Create Payment VA
     * @param $paymentsId
     * @param null $reference
     * @param null $vaType
     * @param null $vaNumber
     * @param string $status
     * @return \stdClass
     */
    public static function createPayment($paymentsId,$reference=null,$vaType=null,$vaNumber=null,$status='WAITING'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $paymentDb = new self();
        $paymentDb->payments_id = $paymentsId;
        $paymentDb->payment_id = $reference;
        $paymentDb->va_type = $vaType;
        $paymentDb->va_number = $vaNumber;
        $paymentDb->status = $status;
        $paymentDb->save();

        $response->isSuccess = true;
        return $response;
    }
}
