<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
    // set table
    protected $table = 'errors';

    public static function insertError($code,$url,$message,$request=[],$details=[]){
        // check error exist
        $errorDb = self::where('code',$code)->where('url',$url)->where('message',$message)->where('status','UNSOLVED')->first();
        if (!$errorDb){
            // if not exist insert new error
            $errorDb = new self();
            $errorDb->code = $code;
            $errorDb->url = $url;
            $errorDb->message = $message;
            $errorDb->status = 'UNSOLVED';
            $errorDb->save();
        }
        $errorDb = self::find($errorDb->id);
        // insert new details
        $detailDb = new ErrorDetail();
        $detailDb->errors_id = $errorDb->id;
        $detailDb->request = json_encode($request);
        $detailDb->details = json_encode($details);
        $detailDb->save();

        $occurrence = $errorDb->occurrence;
        $occurrence = $occurrence+1;
        $errorDb->occurrence = $occurrence;
        $errorDb->save();
    }

    /*Relationship*/
    public function details(){
        return $this->hasMany(ErrorDetail::class,'id','errors_id');
    }
}
