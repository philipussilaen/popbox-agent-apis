<?php

namespace App\Models;

use App\Http\Helpers\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserSession extends Model
{
    protected $table = 'user_sessions';

    /**
     * Check Valid User Session
     * @param $userId
     * @param $sessionId
     * @return \stdClass
     */
    public static function checkUserSession($userId,$sessionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        if (empty($userId)){
            $response->errorMsg = 'Empty user ID';
            return $response;
        }

        // check on user session table
        $userSessionDb = self::where('user_id',$userId)
            ->where('session_id',$sessionId)
            ->orderBy('created_at','desc')
            ->first();
        if (!$userSessionDb) {
            $response->errorMsg = 'Sesi Anda tidak Valid. Silahkan Login/Masuk kembali';
            return $response;
        }

        if ($userSessionDb->status != 'valid'){
            $response->errorMsg = 'Sesi Anda tidak Valid. Silahkan Login/Masuk Kembali';
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    public static function createUserSession($userId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errormsg = null;
        $response->sessionId = null;

        $userDb = User::find($userId);
        if (!$userDb){
            $response->errormsg = 'Invalid User';
            return $response;
        }

        // invalid all session based on user ID
        self::invalidUserSession($userId);

        // create user session
        $randomOne = Helper::generateRandomString(50);
        $phone = $userDb->phone;
        $randomTwo = Helper::generateRandomString(50);

        $sessionId = $randomOne.$phone.$randomTwo;

        // save to DB
        $userSessionDb = new UserSession();
        $userSessionDb->user_id = $userId;
        $userSessionDb->session_id = $sessionId;
        $userSessionDb->status = 'valid';
        $userSessionDb->save();

        $response->isSuccess = true;
        $response->sessionId = $sessionId;
        return $response;
    }

    /**
     * Invalid Active Session based on user ID
     * @param $userId
     */
    private static function invalidUserSession($userId){
        DB::table('user_sessions')
            ->where('user_id',$userId)
            ->where('status','valid')
            ->update([
                'status' => 'invalid'
            ]);
    }
}
