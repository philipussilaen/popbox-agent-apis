<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTransfer extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * get Available Bank
     * @return array
     */
    public static function getAvailableBanks(){
        $availableBanks = [];
        $tmp = new \stdClass();
        $tmp->id = "BCA1";
        $tmp->bank_name = 'Bank Central Asia (BCA)';
        $tmp->bank_code = 'BCA';
        $tmp->bank_account = '5260-35-8822';
        $tmp->bank_user = 'PT. Popbox Asia Services';
        $tmp->branch = 'Grand Slipi Tower, Jakarta';
        $availableBanks[] = $tmp;

        $tmp = new \stdClass();
        $tmp->id = "MANDIRI1";
        $tmp->bank_name = 'Bank Mandiri';
        $tmp->bank_code = 'MANDIRI';
        $tmp->bank_account = '165-000-91-2222-8';
        $tmp->bank_user = 'PT. Popbox Asia Services';
        $tmp->branch = 'Grand Slipi Tower, Jakarta';
        $availableBanks[] = $tmp;
        return $availableBanks;
    }

    /**
     * Create Payment Transfer
     * @param $paymentId
     * @param $bankId
     * @return \stdClass
     */
    public static function createPaymentTransfer($paymentId,$bankId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get available banks
        $selectedBank = null;
        foreach (self::getAvailableBanks() as $bank){
            if ($bank->id == $bankId){
                $selectedBank = $bank;
            }
        }
        if (empty($selectedBank)){
            $response->errorMsg = "Invalid Bank Id";
            return $response;
        }

        $destinationBank = $selectedBank->bank_name;
        $destinationAccount = $selectedBank->bank_account;

        // insert to database
        $paymentDb = new self();
        $paymentDb->payments_id = $paymentId;
        $paymentDb->destination_bank = $destinationBank;
        $paymentDb->destination_account = $destinationAccount;
        $paymentDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Submit confirmation from agent
     * @param $transactionRef
     * @param $input
     * @return \stdClass
     */
    public static function submitConfirmation($transactionRef,$input){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $transactionDb = Transaction::where('reference',$transactionRef)->first();
        if ($transactionDb->type!='topup'){
            $response->errorMsg = 'Invalid Top Up Transaction';
            return $response;
        }
        if ($transactionDb->status!='WAITING'){
            $response->errorMsg = "Invalid Status. $transactionDb->status";
            return $response;
        }
        if ($transactionDb->payment->method->code!='transfer'){
            $response->errorMsg = "Not Bank Transfer Method";
            return $response;
        }
        // change transaction to pending
        $transactionDb = Transaction::find($transactionDb->id);
        $transactionDb->status = 'PENDING';
        $transactionDb->save();

        // change payment to pending
        $paymentDb = Payment::find($transactionDb->payment->id);
        $paymentDb->status = 'PENDING';
        $paymentDb->save();

        $senderBank = $input['sender_bank'];
        $senderName = $input['sender_name'];
        $remarks = empty($input['remarks']) ?  null : $input['remarks'];
        $picture = empty($input['picture']) ?  null : $input['picture'];

        $paymentId = $paymentDb->id;
        $transferDb = self::where('payments_id',$paymentId)->first();
        $transferDb = self::find($transferDb->id);
        $transferDb->sender_bank = $senderBank;
        $transferDb->sender_name = $senderName;
        $transferDb->remarks = $remarks;
        // save picture
        if (!empty($picture)){
            // generate filename
            $filename = $transactionRef.'-'.date('Ymd').'.jpg';
            $path = public_path()."/agent/transfer/$filename";
            $img = base64_decode($picture);
            $saving = file_put_contents($path,$img);
            if (!$saving){
                $response->errorMsg = "Failed to Save Transaction Picture";
                return $response;
            }
            $transferDb->picture = $filename;
        }
        $transferDb->save();

        $response->isSuccess = true;
        return $response;
    }

    /*================== Relationship ==================*/
    public function payment(){
        return $this->hasOne(Payment::class,'payments_id','id');
    }
}
