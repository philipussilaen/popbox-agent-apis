<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $table = 'transaction_histories';

    /**
     * Create Transaction histories
     * @param $transactionId
     * @param string $user
     * @param string $status
     * @param null $remark
     * @return \stdClass
     */
    public static function createNewHistory($transactionId,$user='',$status='',$remark=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $data = new self();
        $data->transactions_id = $transactionId;
        $data->user = $user;
        $data->status = $status;
        $data->remarks = $remark;
        $data->save();

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function transaction(){
        return $this->belongsTo(Transaction::class,'transactions_id','id');
    }
}
