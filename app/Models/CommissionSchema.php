<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommissionSchema extends Model
{
    // set table
    protected $table = 'commission_schemas';

    /**
     * calculate schema
     * @param $product
     * @param $basicPrice
     * @param $publishPrice
     * @param int $profit
     * @return \stdClass
     */
    public static function calculateSchema($product,$basicPrice,$publishPrice,$profit=0){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->commission = 0;
        $response->commissionId = null;

        // nowDate
        $nowDate = date('Y-m-d H:i:s');

        // get last active commission schema
        $schemaDb = self::where('product',$product)
            ->where('status',1)
            ->where('start_date','<=',$nowDate)
            ->where('end_date','>=',$nowDate)
            ->orderBy('created_at','desc')
            ->get();
        if (!$schemaDb){
            $response->errorMsg = 'Commission Schema Not Found';
            return $response;
        }

        // calculate schema
        foreach ($schemaDb as $schema) {
            $schemaType = $schema->type;
            $schemaRule = $schema->rule;
            $schemaTransactionRule = $schema->transaction_rule;
            $schemaValue = $schema->values;

            $price = 0;
            if ($schemaRule == 'basic') $price = $basicPrice;
            elseif ($schemaRule == 'profit') $price = $profit;
            else $price = $publishPrice;

            // check if there is transaction rule
            if (!empty($schemaTransactionRule)){
                #get matched rule
                $schemaTransactionRule = json_decode($schemaTransactionRule);
                $transactionValue = reset($schemaTransactionRule);
                $transactionOperator = key($schemaTransactionRule);
                #if transaction rule between
                if ($transactionOperator == 'between'){
                    $transactionValue = explode('-',$transactionValue);
                    #if price between the transaction rule
                    if ($price >= $transactionValue[0] && $price <= $transactionValue[1]){
                        $response->commissionId = $schema->id;
                        $response->commission = self::calculate($schemaType,$schemaValue,$price);
                        break;
                    }
                } elseif ($transactionOperator == '>'){
                    if ($price > $transactionValue) {
                        $response->commissionId = $schema->id;
                        $response->commission = self::calculate($schemaType,$schemaValue,$price);
                        break;
                    }
                } elseif ($transactionOperator == '>='){
                    if ($price >= $transactionValue) {
                        $response->commissionId = $schema->id;
                        $response->commission = self::calculate($schemaType,$schemaValue,$price);
                        break;
                    }
                } elseif ($transactionOperator == '<'){
                    if ($price < $transactionValue) {
                        $response->commissionId = $schema->id;
                        $response->commission = self::calculate($schemaType,$schemaValue,$price);
                        break;
                    }
                } elseif ($transactionOperator == '<='){
                    if ($price <= $transactionValue) {
                        $response->commissionId = $schema->id;
                        $response->commission = self::calculate($schemaType,$schemaValue,$price);
                        break;
                    }
                }
            }
            // if transaction rule not available
            else {
                $response->commissionId = $schema->id;
                $response->commission = self::calculate($schemaType,$schemaValue,$price);
                break;
            }
        }
        $response->commission = round($response->commission);
        $response->isSuccess = true;
        return $response;
    }

    /**
     * calculate schema by schema Id
     * @param $commissionId
     * @param $basicPrice
     * @param $publishPrice
     * @param int $profit
     * @return \stdClass
     */
    public static function calculateById($commissionId,$basicPrice,$publishPrice,$profit=0){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->commission = 0;

        // find commission schema
        $schemaDb = self::find($commissionId);
        if (!$schemaDb){
            $response->errorMsg = 'Invalid Commission Schema Id';
            return $response;
        }
        $schemaType = $schemaDb->type;
        $schemaValue = $schemaDb->values;
        $price = $publishPrice;
        if (!empty($rule)){
            if ($rule=='publish') $price = $publishPrice;
            if ($rule=='basic') $price = $basicPrice;
            if ($rule=='profit') $price = $profit;
        }

        $amount = self::calculate($schemaType,$schemaValue,$price);
        if ($amount > $price){
            $response->errorMsg = 'Invalid Commission Amount and Price';
            return $response;
        }
        $response->commission = $amount;
        $response->isSuccess = true;
        return $response;
    }

    /**
     * Calculate Schema New
     * @param $param
     * @param $basicPrice
     * @param $publishPrice
     * @param int $profit
     * @return \stdClass
     */
    public static function newCalculateSchema($param,$basicPrice,$publishPrice,$profit=0){
        Log::logFile("","commission","Begin New Calculate Schema");
        // default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->commission = 0;
        $response->commissionId = null;

        // get active commission schema, order by priority and created_date
        $nowDateTime = date('Y-m-d H:i:s');
        $commissionSchemaDb = CommissionSchema::where('status','=','1')
            ->where('start_date','<=',$nowDateTime)
            ->where('end_date','>=',$nowDateTime)
            ->where('approval_status','approved')
            ->orderBy('priority','asc')
            ->orderBy('created_at','desc')
            ->get();

        $selectedSchema = null;
        foreach ($commissionSchemaDb as $scheme){
            $commissionSchemaId = $scheme->id;
            // check rule per commission schema
            Log::logFile("","commission","======== Begin Check Commission Schema ".$commissionSchemaId." =============");
            Log::logFile("","commission","Params ".json_encode($param));
            $check = self::checkRuleCommission($commissionSchemaId,$param);
            Log::logFile("","commission","End Check Commission Schema ".$check->errorMsg);
            if ($check->isSuccess){
                $selectedSchema = $scheme;
                break;
            }
        }

        if (empty($selectedSchema)){
            $response->errorMsg = 'Commission Schema Not Found';
            Log::logFile("","commission","End New Calculate Schema ".$response->errorMsg);
            return $response;
        }
        $schemaType = $selectedSchema->type;
        $schemaValue = $selectedSchema->values;
        $price = $publishPrice;
        if (!empty($rule)){
            if ($rule=='publish') $price = $publishPrice;
            if ($rule=='basic') $price = $basicPrice;
            if ($rule=='profit') $price = $profit;
        }

        $amount = self::calculate($schemaType,$schemaValue,$price);
        if ($amount > $price){
            $response->errorMsg = 'Invalid Commission Amount and Price';
            Log::logFile("","commission","End New Calculate Schema ".$response->errorMsg);
            return $response;
        }

        $response->commission = $amount;
        $response->commissionId = $selectedSchema->id;
        $response->isSuccess = true;
        Log::logFile("","commission","End New Calculate Schema ".json_encode($response));
        return $response;
    }

    /*============= Private Function =============*/

    /**
     * Check Rule Commission
     * @param $schemaId
     * @param array $param
     * @return \stdClass
     */
    private static function checkRuleCommission($schemaId, $param=[]){
        // response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        Log::logFile("","commission","Begin Check Commission Rule Schema ".$schemaId);
        // get all available rules
        $commissionRulesDb = CommissionRule::where('commission_schema_id',$schemaId)
            ->join('available_parameters','available_parameters.id','=','commission_rules.available_parameter_id')
            ->select('commission_rules.*','available_parameters.name','available_parameters.param_name')
            ->get();
        $missingParam = [];
        $failedParam = [];

        foreach ($commissionRulesDb as $item) {
            // check if param and rule exist
            if (!isset($param[$item->param_name])){
                $missingParam[] = $item->param_name;
            }
        }

        if (!empty($missingParam)){
            $tmp = implode(',',$missingParam);
            $errorMsg = "Missing Param : ".$tmp;
            Log::logFile("","commission",$errorMsg);

            $response->errorMsg = $errorMsg;
            return $response;
        }

        // check rule
        foreach ($commissionRulesDb as $item) {
           $paramValue = $param[$item->param_name];
           $ruleName = $item->name;
           $ruleOperator = $item->operator;
           $ruleValue = $item->value;
            Log::logFile("","commission","Check Rule $ruleName $ruleValue '$ruleOperator' $paramValue");

            $checkRulePass= self::checkRulePass($ruleName,$ruleOperator,$ruleValue,$paramValue);
           if (!$checkRulePass) $failedParam[] = $item->name;
        }
        if (!empty($failedParam)){
            $tmp = implode(',',$failedParam);
            $errorMsg = "Failed Param : ".$tmp;
            Log::logFile("","commission",$errorMsg);
            $response->errorMsg = $errorMsg;
            return $response;
        }
        $response->isSuccess = true;
        Log::logFile("","commission","Finish Check Commission Rule Schema ".$schemaId);
        return $response;
    }

    /**
     * Check Rule Scheme Pass
     * @param $ruleName
     * @param $ruleOperator
     * @param $ruleValuere
     * @param $paramValue
     * @return bool
     */
    private static function checkRulePass($ruleName,$ruleOperator,$ruleValue,$paramValue){
        $isPass = false;
        $valueArray = [];
        $multiple = ['between','exist','except'];
        if (in_array($ruleOperator,$multiple)){
            $valueArray = json_decode($ruleValue);
        }
        switch ($ruleName){
            /*==========transaction_type==========*/
            case 'transaction_type' :
                if ($ruleOperator=='='){
                    if($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='exist'){
                    if (in_array($paramValue,$valueArray)) $isPass = true;
                } elseif ($ruleOperator=='except'){
                    if (!in_array($paramValue,$valueArray)) $isPass = true;
                }
                break;
            /*==========transaction_item_type==========*/
            case 'transaction_item_type' :
                if ($ruleOperator=='='){
                    if($paramValue == $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='exist'){
                    if (in_array($paramValue,$valueArray)) $isPass = true;
                } elseif ($ruleOperator=='except'){
                    if (!in_array($paramValue,$valueArray)) $isPass = true;
                }
                break;
            /*=============customer_phone=============*/
            case 'customer_phone_daily' :
                $startDateTime = date('Y-m-d')." 00:00:00";
                $endDateTime = date('Y-m-d')." 23:59:59";
                // count customer phone
                $customerPhone = $paramValue;
                $transactionCount = TransactionItem::join('transactions','transactions.id','=','transaction_items.transaction_id')
                    ->where('transactions.status','PAID')
                    ->where('transaction_items.type','=','pulsa')
                    ->whereBetween('transaction_items.created_at',[$startDateTime,$endDateTime])
                    ->where('params','LIKE',"%$customerPhone%")
                    ->count();

                if ($ruleOperator=='<'){
                    if ($transactionCount < $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='<='){
                    if ($transactionCount <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '='){
                    if ($transactionCount == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>='){
                    if ($transactionCount >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>'){
                    if ($transactionCount > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between'){
                    if (!empty($valueArray[0]) && !empty($valueArray[1])){
                        if ($transactionCount > $valueArray[0] && $transactionCount < $valueArray[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist'){
                    if (in_array($transactionCount,$valueArray)) $isPass = true;
                } elseif ($ruleOperator == 'except'){
                    if (!in_array($transactionCount,$valueArray)) $isPass = true;
                }
                 Log::logFile("","commission","Customer phone daily $transactionCount $ruleOperator $ruleValue");
                break;
            case 'customer_phone_weekly' :
                $startDateTime = date('Y-m-d',strtotime('this Monday'))." 00:00:00";
                $endDateTime = date('Y-m-d',strtotime('this Sunday'))." 23:59:59";
                // count customer phone
                $customerPhone = $paramValue;
                $transactionCount = TransactionItem::join('transactions','transactions.id','=','transaction_items.transaction_id')
                    ->where('transactions.status','PAID')
                    ->where('transaction_items.type','=','pulsa')
                    ->whereBetween('transaction_items.created_at',[$startDateTime,$endDateTime])
                    ->where('params','LIKE',"%$customerPhone%")
                    ->count();

                if ($ruleOperator=='<'){
                    if ($transactionCount < $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='<='){
                    if ($transactionCount <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '='){
                    if ($transactionCount == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>='){
                    if ($transactionCount >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>'){
                    if ($transactionCount > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between'){
                    if (!empty($valueArray[0]) && !empty($valueArray[1])){
                        if ($transactionCount > $valueArray[0] && $transactionCount < $valueArray[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist'){
                    if (in_array($transactionCount,$valueArray)) $isPass = true;
                } elseif ($ruleOperator == 'except'){
                    if (!in_array($transactionCount,$valueArray)) $isPass = true;
                }
                break;
            case 'customer_phone_monthly' :
                $startDateTime = date('Y-m-1')." 00:00:00";
                $endDateTime = date('Y-m-t')." 23:59:59";
                // count customer phone
                $customerPhone = $paramValue;
                $transactionCount = TransactionItem::join('transactions','transactions.id','=','transaction_items.transaction_id')
                    ->where('transactions.status','PAID')
                    ->where('transaction_items.type','=','pulsa')
                    ->whereBetween('transaction_items.created_at',[$startDateTime,$endDateTime])
                    ->where('params','LIKE',"%$customerPhone%")
                    ->count();

                if ($ruleOperator=='<'){
                    if ($transactionCount < $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='<='){
                    if ($transactionCount <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '='){
                    if ($transactionCount == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>='){
                    if ($transactionCount >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>'){
                    if ($transactionCount > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between'){
                    if (!empty($valueArray[0]) && !empty($valueArray[1])){
                        if ($transactionCount > $valueArray[0] && $transactionCount < $valueArray[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist'){
                    if (in_array($transactionCount,$valueArray)) $isPass = true;
                } elseif ($ruleOperator == 'except'){
                    if (!in_array($transactionCount,$valueArray)) $isPass = true;
                }
                break;
            /*=================user_id=================*/
            case 'transaction_count_daily' :
                $startDateTime = date('Y-m-d')." 00:00:00";
                $endDateTime = date('Y-m-d')." 23:59:59";
                // count customer phone
                $userId = $paramValue;
                $transactionCount = Transaction::whereBetween('created_at',[$startDateTime,$endDateTime])
                    ->whereIn('type',['purchase','payment','delivery'])
                    ->where('status','PAID')
                    ->where('users_id',$userId)
                    ->count();

                if ($ruleOperator=='<'){
                    if ($transactionCount < $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='<='){
                    if ($transactionCount <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '='){
                    if ($transactionCount == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>='){
                    if ($transactionCount >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>'){
                    if ($transactionCount > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between'){
                    if (!empty($valueArray[0]) && !empty($valueArray[1])){
                        if ($transactionCount > $valueArray[0] && $transactionCount < $valueArray[1]) $isPass = true;
                    }
                }
                break;
            case 'transaction_count_weekly' :
                $nowDateTime = date('Y-m-d',strtotime('this Monday'))." 00:00:00";
                $endDateTime = date('Y-m-d',strtotime('this Sunday'))." 23:59:59";
                // count customer phone
                $userId = $paramValue;
                $transactionCount = Transaction::whereBetween('created_at',[$nowDateTime,$endDateTime])
                    ->whereIn('type',['purchase','payment','delivery'])
                    ->where('status','PAID')
                    ->where('users_id',$userId)
                    ->count();

                if ($ruleOperator=='<'){
                    if ($transactionCount < $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='<='){
                    if ($transactionCount <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '='){
                    if ($transactionCount == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>='){
                    if ($transactionCount >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>'){
                    if ($transactionCount > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between'){
                    if (!empty($valueArray[0]) && !empty($valueArray[1])){
                        if ($transactionCount > $valueArray[0] && $transactionCount < $valueArray[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist'){
                    if (in_array($transactionCount,$valueArray)) $isPass = true;
                } elseif ($ruleOperator == 'except'){
                    if (!in_array($transactionCount,$valueArray)) $isPass = true;
                }
                break;
            case 'transaction_count_monthly' :
                $startDateTime = date('Y-m-1')." 00:00:00";
                $endDateTime = date('Y-m-t')." 23:59:59";
                // count customer phone
                $userId = $paramValue;
                $transactionCount = Transaction::whereBetween('created_at',[$startDateTime,$endDateTime])
                    ->whereIn('type',['purchase','payment','delivery'])
                    ->where('status','PAID')
                    ->where('users_id',$userId)
                    ->count();

                if ($ruleOperator=='<'){
                    if ($transactionCount < $ruleValue) $isPass = true;
                } elseif ($ruleOperator=='<='){
                    if ($transactionCount <= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '='){
                    if ($transactionCount == $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>='){
                    if ($transactionCount >= $ruleValue) $isPass = true;
                } elseif ($ruleOperator == '>'){
                    if ($transactionCount > $ruleValue) $isPass = true;
                } elseif ($ruleOperator == 'between'){
                    if (!empty($valueArray[0]) && !empty($valueArray[1])){
                        if ($transactionCount > $valueArray[0] && $transactionCount < $valueArray[1]) $isPass = true;
                    }
                } elseif ($ruleOperator == 'exist'){
                    if (in_array($transactionCount,$valueArray)) $isPass = true;
                } elseif ($ruleOperator == 'except'){
                    if (!in_array($transactionCount,$valueArray)) $isPass = true;
                }
                break;
            case 'locker_id' :
                $userId = $paramValue;
                $userDb = User::find($userId);
                if ($userDb){
                    $lockerId = $userDb->locker_id;
                    if ($lockerId){
                        if ($ruleOperator=='='){
                            if($lockerId == $ruleValue) $isPass = true;
                        }
                        elseif ($ruleOperator=='exist'){
                            if (in_array($lockerId,$valueArray)) $isPass = true;
                        }
                        elseif ($ruleOperator=='except'){
                            if (!in_array($lockerId,$valueArray)) $isPass = true;
                        }
                    }
                }
                break;
            /*===========sepulsa_product_id===========*/
            case 'sepulsa_product_id':
                $sepulsaProductId = $paramValue;
                if ($ruleOperator=='='){
                    if($sepulsaProductId == $ruleValue) $isPass = true;
                }
                elseif ($ruleOperator=='exist'){
                    if (in_array($sepulsaProductId,$valueArray)) $isPass = true;
                }
                elseif ($ruleOperator=='except'){
                    if (!in_array($sepulsaProductId,$valueArray)) $isPass = true;
                }
                break;
        }
        return $isPass;
    }

    /**
     * Calculate based on type
     * @param $type
     * @param $values
     * @param $price
     * @return int
     */
    private static function calculate($type,$values,$price){
        $amount = 0;
        // if type percent
        if ($type=='percent'){
            $amount = $price*$values/100;
        } elseif ($type=='fixed') {
            $amount = $values;
        }
        $amount = floor($amount);
        return $amount;
    }

    /*===============Relationship===============*/
    public function commissionRules(){
        return $this->hasMany(CommissionRule::class);
    }
}
