<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualLockerGroup extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'groups';
}
