<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DimoCategoryName extends Model
{
    // set connection
    protected $connection = 'popbox_db';
    // set table
    protected $table = 'dimo_product_category_name';
    protected $primaryKey = 'id_category';

    /*Relationship*/
    public function products(){
        return $this->belongsToMany(DimoProduct::class,'dimo_product_category','id_category','id_category');
    }
}
