<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailSepulsaTransaction extends Model
{
    protected $connection= 'popbox_db';
    protected $table = 'detail_sepulsa_transaction';
}
