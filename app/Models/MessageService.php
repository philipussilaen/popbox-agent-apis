<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class MessageService extends Model
{
    protected $table = 'message_service';
    use SoftDeletes;
    
    public static function createMessageNotification($message_id, $module){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        $device = 'android';
        $row_id = 0;
        
        DB::beginTransaction();
        $message_service = DB::connection('mysql')
        ->select("
                select ms.id, ms.title, ms.body_message, ms.url_loc, ms.menu_app, ms.url, ms.image_name, msd.recipient, u.gcm_token
                from message_service ms
                join message_service_detail msd on ms.id = msd.message_service_id
                join users u on u.locker_id = msd.recipient
                where msd.message_service_id = ?
            ",[$message_id]);
        
        if(!empty($message_service)){
            foreach ($message_service as $row){
                if(!empty($row->gcm_token)){
                    
                    $row_id = $row_id + 1;
                    
                    $fcmData = [];
                    $fcmData['id'] = $row_id;
                    $fcmData['timestamp'] = date('Y-m-d H:i:s');
                    $fcmData['img_url'] = env('AGENT_URL').'upload/'.$row->image_name;
                    
                    $notificationFCM =  new NotificationFCM();
                    $notificationFCM->insertNew($module, $row->gcm_token, $device, $row->title, $row->body_message, $fcmData);
                }
            }
        }
        DB::commit();
        
        $response->count = $row_id;
        $response->isSuccess = true;
        return $response;
    }
}
