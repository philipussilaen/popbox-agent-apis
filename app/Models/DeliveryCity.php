<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryCity extends Model
{
    protected $table = 'delivery_cities';

    /*relationship*/
    public function districts(){
        return $this->hasMany(DeliveryDistrict::class);
    }

    public function province(){
        return $this->belongsTo(DeliveryProvince::class,'delivery_province_id','id');
    }
}
