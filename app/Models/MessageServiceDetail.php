<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageServiceDetail extends Model
{
    protected $table = 'message_service_detail';
}
