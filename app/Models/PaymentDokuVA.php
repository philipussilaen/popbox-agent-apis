<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentDokuVA extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'payment_doku_va';

    /**
     * insert Payment Doku VA
     * @param $paymentId
     * @param $vaType
     * @param $vaNumber
     * @param string $status
     * @return \stdClass
     */
    public static function createPaymentDokuVA($paymentId,$vaType,$vaNumber,$status='WAITING'){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $paymentDb = new self();
        $paymentDb->payments_id = $paymentId;
        $paymentDb->va_type = $vaType;
        $paymentDb->va_number = $vaNumber;
        $paymentDb->status = $status;
        $paymentDb->save();

        $response->isSuccess = true;
        return $response;
    }
}
