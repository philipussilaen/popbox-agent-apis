<?php

namespace App\Console\Commands;

use App\Models\NotificationFCM;
use Illuminate\Console\Command;

class NotificationSendFCM extends Command
{
    private $uniqueId = null;
    // function from old app
    protected $headers = array (
        'Content-Type: application/json',
        'Authorization:key=AAAAUYtjWQ0:APA91bGwW1TobozTaxsGAKILcoWzQEJC2hnw2o43wNzdc0z4tC5DoeRmE_UxB_O-Wt8OV90ZWf9ToY9ziBF6L6LPP9FWT9T92VYcfwboi8r19lwqrf1ahu9Lx3vc-vwa3GqIeAh7bUzACfT9JIXSq_xcGbdHZAhkZA',
    );
    protected $is_post = 0;
    protected $FCMUrl = 'https://fcm.googleapis.com/fcm/send';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:send-fcm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send created FCM Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all created fcm notification
        $list = NotificationFCM::where('status','created')->take(200)->get();
        $this->log('Begin FCM Job');
        foreach ($list as $item) {
            $notificationId = $item->id;
            $type = $item->device;
            if ($type == 'ios') $param = $this->sendIos($item);
            else $param = $this->sendAndroid($item);

            $status = 'failed';
            $response = null;

            // send FCM
            $url = $this->FCMUrl;
            $param = json_encode($param);
            $this->log("Send Notification ID: $notificationId $type. $url Param: $param");
            $postData = $this->post_data($url,$param);
            if (empty($postData)){
                $this->log('Failed Push. Continue');
                continue;
            }
            $response = json_encode($postData);
            if ($postData['success']==1){
                $status = 'success';
                $this->log('Success Push. Response: '.$response);
            } else {
                $this->log('Failed Push. Response: '.$response);
            }
            // Update DB
            $dataDB = NotificationFCM::find($notificationId);
            $dataDB->parameter = $param;
            $dataDB->status = $status;
            $dataDB->response = $response;
            $dataDB->save();
        }
        $this->log('Done FCM Job');
    }

    private function sendIos($item){
        // create parameter
        $param = [];
        $param['registration_ids'] = [$item->to];
        $param['priority'] = 'high';
        $data = json_decode($item->data,true);
        $notification = [];
        $notification['title'] = $item->title;
        $notification['body'] = $item->body;
        if (!empty($data['title'])) unset($data['title']);
        if (!empty($data['body'])) unset($data['body']);
        $param['notification'] = $notification;
        $param['data'] = $data;

        return $param;
    }

    private function sendAndroid($item){
        // create parameter
        $param = [];
        $param['registration_ids'] = [$item->to];
        $param['priority'] = 'high';
        $data = json_decode($item->data,true);
        if (empty($data['title'])) $data['title'] = $item->title;
        if (empty($data['body'])) $data['body'] = $item->body;
        $param['data'] = $data;

        return $param;
    }

    /**
     * logging
     * @param $msg
     */
    private function log($msg){
        $uniqueId = $this->uniqueId;
        $msg = "FCM $uniqueId $msg\n";
        $f = fopen(storage_path().'/logs/apps/'.date('Y.m.d.').'log','a');
        fwrite($f,date('H:i:s')." $msg");
        fclose($f);
        return;
    }

    /**
     * Post data to third party apps
     * @param $url
     * @param array $post_data
     * @param array $headers
     * @param array $options
     * @return mixed|null
     */
    private function post_data($url, $post_data = array(), $headers = array(), $options = array()) {
        $result = null;
        $curl = curl_init ();

        if ((is_array ( $options )) && count ( $options ) > 0) {
            $this->options = $options;
        }
        if ((is_array ( $headers )) && count ( $headers ) > 0) {
            $this->headers = $headers;
        }
        if ($this->is_post !== null) {
            $this->is_post = 1;
        }

        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLOPT_POST, $this->is_post );
        curl_setopt ( $curl, CURLOPT_POSTFIELDS, $post_data );
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPHEADER, $this->headers );

        $content = curl_exec ( $curl );
        $response = curl_getinfo ( $curl );
        $result = json_decode ( $content, TRUE );
        curl_close ( $curl );

        $message = "Send FCM $url Param: ".json_encode($post_data)." Response: ".json_encode($result);
        $this->log($message);
        return $result;
    }
}
