<?php

namespace App\Console\Commands;

use App\Http\Helpers\Helper;
use App\Models\Locker;
use App\Models\Log;
use App\Models\VirtualLocker;
use Illuminate\Console\Command;

class LockerNameReferral extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locker:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Locker Name and Generate Referral';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $location = storage_path()."/logs/cron/";
        echo "Get Null Locker Name\n";
        Log::logFile($location,'lockerNameReferral',"Get Null Locker Name");

        $lockerData = Locker::whereNull('locker_name')->get();
        foreach ($lockerData as $item) {
            echo "$item->id Empty Name\n";
            Log::logFile($location,'lockerNameReferral',"$item->id Empty Name");
            // get locker name from PopBox Virtual
            $lockerVirtualDb = VirtualLocker::where('locker_id',$item->id)->first();
            if (!$lockerVirtualDb){
                // if not found continue
                echo "$item->id Not Found in Locker Virtual\n";
                Log::logFile($location,'lockerNameReferral',"$item->id Not Found in Locker Virtual");
                continue;
            }
            // update table
            $lockerDb = Locker::find($item->id);
            $lockerDb->locker_name = $lockerVirtualDb->locker_name;
            $lockerDb->save();
            echo "$item->id Locker Name :  $lockerVirtualDb->locker_name\n";
            Log::logFile($location,'lockerNameReferral',"$item->id Locker Name :  $lockerVirtualDb->locker_name");
        }
        Log::logFile($location,'lockerNameReferral',"===================================================");
        echo "======================================";
        echo "Get Null Locker Name\n";
        Log::logFile($location,'lockerNameReferral',"Get Null Locker Name");

        $lockerData = Locker::whereNull('referral_code')->get();
        foreach ($lockerData as $item) {
            Log::logFile($location,'lockerNameReferral',"$item->id Empty Referral");
            echo "$item->id Empty Referral \n";
            // get name member
            $name = $item->locker_name;
            if (empty($name)) continue;
            // clear double space
            $name = preg_replace('/\s+/', ' ',$name);
            // explode per word
            $words = explode(" ", $name);
            $firstName = '';
            if (!empty($words)){
                foreach ($words as $w) {
                    $firstName .= $w[0];
                }
            }

            $firstName = strtoupper($firstName);
            // generate random string
            $random = Helper::generateRandomString(3);

            $referralCode = $firstName.$random;

            // update table
            $lockerDb = Locker::find($item->id);
            $lockerDb->referral_code = $referralCode;
            $lockerDb->save();
            echo "$item->id Referral Code : $referralCode\n";
            Log::logFile($location,'lockerNameReferral',"$item->id Referral Code : $referralCode");
        }
    }
}
