<?php

namespace App\Console\Commands;

use App\Models\CommissionSchema;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TransactionCommissionReference extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:reference';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Transaction and Items with Reference and Commission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Get Transaction Items with Null Commission Amount and Have Commission ID\n";
        $transactionItemDb = TransactionItem::with('transaction')
            ->whereNotNull('commission_schemas_id')
            ->whereNull('commission_amount')
            ->where('type','<>','popshop')
            ->take(60)
            ->get();
        $purchaseList = ['pulsa','popshop'];
        $paymentList = ['electricity','electricity_postpaid','pdam','bpjs_kesehatan','telkom_postpaid'];

        DB::beginTransaction();
        foreach ($transactionItemDb as $item){
            echo "Process ID: $item->id > $item->name\n";
            // calculate schema
            $commissionId = null;
            $commissionAmount = null;
            $transactionType = 'purchase';
            if (in_array($item->type,$purchaseList)) $transactionType = 'purchase';
            elseif ($item->type == 'delivery') $transactionType = 'delivery';
            elseif (in_array($item->type,$paymentList)) $transactionType = 'payment';
            if (empty($item->transaction)) {
                continue;
            }
            $userId = $item->transaction->users_id;

            if ($item->type == 'pulsa'){
                $itemParams = json_decode($item->params);
                $params = [];
                $params['transaction_type'] = $transactionType;
                $params['transaction_item_type'] = 'pulsa';
                $params['user_id'] = $userId;
                $params['customer_phone'] = $itemParams->phone;
                $basicPrice = $item->price;
                $publishPrice = $item->price;
                $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
                if ($commission->isSuccess){
                    $commissionId = $commission->commissionId;
                    $commissionAmount = $commission->commission;
                }
            } else {
                $itemParams = json_decode($item->params);
                $params = [];
                $params['transaction_type'] = $transactionType;
                $params['transaction_item_type'] = $item->type;
                $params['user_id'] = $userId;
                $basicPrice = $item->price;
                $publishPrice = $item->price;
                $commission = CommissionSchema::newCalculateSchema($params,$basicPrice,$publishPrice);
                if ($commission->isSuccess){
                    $commissionId = $commission->commissionId;
                    $commissionAmount = $commission->commission;
                }
            }

            $itemDb = TransactionItem::find($item->id);
            $itemDb->commission_amount = $commissionAmount;
            $itemDb->save();
            echo "Save $commissionAmount\n";
        }
        DB::commit();

       /* $beginDate = date('Y-m-d H:i:s',strtotime("-29 weeks")); #29
        $endDate = date('Y-m-d H:i:s',strtotime('-27 weeks')); #27
        echo "Get Transaction Date $beginDate - $endDate\n";
        $transactionDb = Transaction::whereIn('type',['purchase','payment'])
            ->whereBetween('created_at',[$beginDate,$endDate])
            ->take(250)
            ->orderBy('id','asc')
            ->get();*/

       /* DB::beginTransaction();
        foreach ($transactionDb as $item){
            $reference = $item->reference;
            $type = $item->type;
            $status = $item->status;
            $transactionId = $item->id;
            echo "Process $reference $type $status\n";

            $tmpBeginDate = $item->created_at;
            // search commission
            $commissionTransactionDb = Transaction::where('type','=','commission')
                ->where('description','LIKE',"%$reference%")
                ->whereBetween('created_at',[$beginDate,$tmpBeginDate])
                ->first();
            if (!$commissionTransactionDb){
                echo "Commission Not Found \n";
                continue;
            }
            $commissionReference = $commissionTransactionDb->reference;
            $commissionId = $commissionTransactionDb->id;
            echo "Commission $commissionReference $commissionId\n";

            // update commission transaction
            $commissionTransactionDb = Transaction::find($commissionId);
            $commissionTransactionDb->transaction_id_reference = $transactionId;
            $commissionTransactionDb->status = $status;
            $commissionTransactionDb->save();

            echo "Update Commission : $transactionId :  $status\n";
            echo "============ \n";
        }
        DB::commit();*/
    }
}
