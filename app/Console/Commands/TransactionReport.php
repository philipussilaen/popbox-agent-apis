<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\TransactionLocationReport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TransactionReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Transaction Report';

    protected $lastDate = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->log('Begin Transaction Report');

       // get last transaction Date
        $firstDateTransaction = "2017-05-02 00:00:00";

        $transactionReport = TransactionLocationReport::orderBy('id','desc')
            ->first();

        if ($transactionReport){
            $firstDateTransaction = $transactionReport->transaction_datetime;
        }
        DB::enableQueryLog();


        $stopDate = new \DateTime($firstDateTransaction);
        $stopDate->modify("+1 day");
        $stopDate = $stopDate->format("Y-m-d 23:59:59");
        // get transaction
        $transactionItemDb = DB::table('transactions as t')
            ->join('transaction_items as ti','t.id','=','ti.transaction_id')
            ->join('users as u','u.id','=','t.users_id')
            ->leftJoin('popbox_virtual.lockers as l','l.locker_id','=','u.locker_id')
            ->leftJoin('popbox_virtual.cities as c','c.id','=','l.cities_id')
            ->leftJoin('popbox_virtual.provinces as p','p.id','=','c.provinces_id')
            ->whereBetween('t.created_at',[$firstDateTransaction,$stopDate])
            ->whereIn('t.type',['purchase','payment'])
            ->whereNull('t.deleted_at')
            ->whereNull('ti.deleted_at')
            ->where('t.status','PAID')
            ->select(DB::raw('l.locker_id, l.locker_name, p.province_name, c.city_name, l.district_name, l.sub_district_name,t.id as "transaction_id", ti.id as "transaction_item_id", ti.name, ti.type, ti.reference, JSON_EXTRACT(ti.params, "$.amount") as quantity,JSON_EXTRACT(ti.params, "$.product_id") as product_id,JSON_EXTRACT(ti.params, "$.sku") as sku,t.total_price, ti.price, ti.created_at '))
            ->orderBy('ti.id','asc')
            ->get();
        foreach ($transactionItemDb as $item) {
            $lockerId = $item->locker_id;
            $transactionId = $item->transaction_id;
            $transactionItemId = $item->transaction_item_id;
            $reference = $item->reference;
            $type = $item->type;
            $name = $item->name;
            $quantity = $item->quantity;
            $price = $item->price;
            $transactionDateTime = $item->created_at;
            $productId = $item->product_id;
            if ($type == 'admin_fee') continue;

            $name = preg_replace('/\s+/', ' ', $name);

            $category = 'digital';
            $paymentList = ['bpjs_kesehatan','electricity','electricity_postpaid','pdam','telkom_postpaid'];

            if ($type == 'pulsa' || in_array($type,$paymentList)) $quantity = 1;
            if ($type == 'popshop') {
                $productId = $item->sku;
                $category = 'non_digital';
                // get popshop category
                $popShopDetail = DB::connection('popbox_db')
                    ->table('order_products as op')
                    ->join('dimo_products as dp','dp.sku','=','op.sku')
                    ->join('dimo_product_category as dpc','dpc.product_sku','=','dp.sku')
                    ->join('dimo_product_category_name as dpcn','dpcn.id_category','=','dpc.id_category')
                    ->where('op.invoice_id',$reference)
                    ->where('dpcn.category_name','LIKE',"%Grocery%")
                    ->select(DB::raw('op.invoice_id, op.sku, dp.name, dpcn.category_name'))
                    ->first();

                if ($popShopDetail){
                    $type = $popShopDetail->category_name;
                    if (strrpos($type,"Grocery") !== false) {
                        $type = str_replace('Grocery : ', "", $type);
                    }
                    if (strrpos($type,'Promo')) {
                        $type = $this->popshopType($name);
                    }
                } else {
                   $type = $this->popshopType($name);
                }

                $type = str_replace(' ', '', $type);

                if ($type == 'popshop') dd($item);
                if ($type == '#Promo') dd($item);
            }
            if (in_array($type,$paymentList)){
                $price = $item->total_price;
                if ($type == 'electricity') $productId = ltrim($item->name);
                if ($type == 'electricity_postpaid') $productId = 'electricity_postpaid';
                if ($type == 'bpjs_kesehatan') $productId = 'bpjs_kesehatan';
                if ($type == 'pdam') $productId = 'pdam';
                if ($type == 'telkom_postpaid') $productId = 'telkom_postpaid';

                if ($type == 'electricity_postpaid') $name = 'electricity_postpaid';
                if ($type == 'bpjs_kesehatan') $name = 'bpjs_kesehatan';
                if ($type == 'pdam') $name = 'pdam';
                if ($type == 'telkom_postpaid') $name = 'telkom_postpaid';
            }

            if (empty($productId)) dd($item);

            $quantity = str_replace('"','',$quantity);
            $productId = str_replace('"','',$productId);

            // check
            $check = TransactionLocationReport::where('transaction_item_id',$transactionItemId)->first();
            if ($check){
                $this->log("Exist $transactionItemId $reference $category $type $name $quantity $price $transactionDateTime");
                continue;
            }
            $this->log("$lockerId $transactionItemId $reference $category $type $name $quantity $price $transactionDateTime");

            DB::beginTransaction();

            $reportDb = new TransactionLocationReport();
            $reportDb->locker_id = $lockerId;
            $reportDb->transaction_id = $transactionId;
            $reportDb->transaction_item_id = $transactionItemId;
            $reportDb->reference = $reference;
            $reportDb->category = $category;
            $reportDb->type = $type;
            $reportDb->name = $name;
            $reportDb->product_id = $productId;
            $reportDb->quantity = (int)$quantity;
            $reportDb->price = $price;
            $reportDb->transaction_datetime = $transactionDateTime;
            $reportDb->save();

            DB::commit();
        }
    }

    private function log($message){
        echo "$message\n";
        $location = storage_path()."/logs/cron/";
        Log::logFile($location,'transaction_report',"$message");
    }

    private function popshopType($name){
        $type = 'popshop';
        $name = strtoupper($name);
        // set type
        if (strpos($name,'INDOMIE') !== false) $type = 'Makanan';
        if (strpos($name,'BIMOLI POUCH') !== false) $type = 'Makanan';
        if (strpos($name,'SAMBAL IF PEDAS TANGGUNG 275 ML') !== false) $type = 'Makanan';
        if (strpos($name,'BUMBU RACIK') !== false) $type = 'Makanan';
        if (strpos($name,'QTELA') !== false) $type = 'Makanan';
        if (strpos($name,'JETZ ') !== false) $type = 'Makanan';
        if (strpos($name,'POP MIE') !== false) $type = 'Makanan';
        if (strpos($name,'BUMBU RACIK') !== false) $type = 'Makanan';
        if (strpos($name,'SARIMI') !== false) $type = 'Makanan';
        if (strpos($name,'WONDERLAND') !== false) $type = 'Makanan';
        if (strpos($name,'POPMIE') !== false) $type = 'Makanan';
        if (strpos($name,'CHITATO') !== false) $type = 'Makanan';
        if (strpos($name,'TRENZ') !== false) $type = 'Makanan';
        if (strpos($name,'CHOCOLATOS') !== false) $type = 'Makanan';
        if (strpos($name,'RICHEESE') !== false) $type = 'Makanan';
        if (strpos($name,'PILUS') !== false) $type = 'Makanan';
        if (strpos($name,'WAFER') !== false) $type = 'Makanan';
        if (strpos($name,'Mie') !== false) $type = 'Makanan';
        if (strpos($name,'ROYCO') !== false) $type = 'Makanan';
        if (strpos($name,'BUMBU') !== false) $type = 'Makanan';
        if (strpos($name,'SAKURA MI') !== false) $type = 'Makanan';
        if (strpos($name,'BIMOLI') !== false) $type = 'Makanan';
        if (strpos($name,'LAYS') !== false) $type = 'Makanan';
        if (strpos($name,'BANGO') !== false) $type = 'Makanan';
        if (strpos($name,'CHEETOS') !== false) $type = 'Makanan';
        if (strpos($name,'NUTRIJEL') !== false) $type = 'Makanan';
        if (strpos($name,'CHIKI') !== false) $type = 'Makanan';
        if (strpos($name,'MIE TELUR') !== false) $type = 'Makanan';

        if ($type!='popshop') {
            return $type;
        }


        if (strpos($name,'CLUB') !== false) $type = 'Minuman';
        if (strpos($name,'INDOMILK') !== false) $type = 'Minuman';
        if (strpos($name,'FRUITAMIN') !== false) $type = 'Minuman';
        if (strpos($name,'TEH PUCUK ') !== false) $type = 'Minuman';
        if (strpos($name,'AQUA') !== false) $type = 'Minuman';
        if (strpos($name,'CAFELA') !== false) $type = 'Minuman';
        if (strpos($name,'ICHI OCHA') !== false) $type = 'Minuman';
        if (strpos($name,'ICHI OCA') !== false) $type = 'Minuman';
        if (strpos($name,'GOOD DAY') !== false) $type = 'Minuman';
        if (strpos($name,'YOU C') !== false) $type = 'Minuman';
        if (strpos($name,'WHITE COFFEE') !== false) $type = 'Minuman';
        if (strpos($name,'KAPAL API') !== false) $type = 'Minuman';

        if ($type!='popshop') {
            return $type;
        }

        if (strpos($name,'GG SURYA') !== false) $type = 'Rokok';
        if (strpos($name,'GUDANG GARAM') !== false) $type = 'Rokok';
        if (strpos($name,'GG FILTER') !== false) $type = 'Rokok';
        if (strpos($name,'GG INTERNATIONAL') !== false) $type = 'Rokok';

        if (strpos($name,'SUNLIGHT') !== false) $type = 'RumahTangga';
        if (strpos($name,'DOVE') !== false) $type = 'RumahTangga';
        if (strpos($name,'SABUN MANDI') !== false) $type = 'RumahTangga';


        return $type;
    }
}
