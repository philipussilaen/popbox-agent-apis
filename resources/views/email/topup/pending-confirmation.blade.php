<!DOCTYPE html>
<html>
<head>
	<title>Bank Transfer Pending</title>
</head>
<body>
	<p>
		Proses Transfer Bank <b>Pending</b> : <b>{{ $data->reference }}</b>
	</p>
	<table>
		<tr>
			<td>Agent</td>
			<td>{{ $data->agent }} - {{ $data->locker }}</td>
		</tr>
		<tr>
			<td>Description</td>
			<td>{{ $data->description }}</td>
		</tr>
		<tr>
			<td>Tanggal</td>
			<td>{{ $data->updated_at }}</td>
		</tr>
		<tr>
			<td>Bank Tujuan</td>
			<td>
				<p>
					Bank Tujuan : <strong>{{ $data->payment_detail->destination_bank }}</strong> <br>
					Bank Account : <strong>{{ $data->payment_detail->destination_account }}</strong> <br>
				</p>
			</td>
		</tr>
		<tr>
			<td>Bank Pengirim</td>
			<td>
				<p>
					Nama Pengirim : <strong>{{ $data->payment_detail->sender_name }}</strong> <br>
					Nama Bank : <strong>{{ $data->payment_detail->sender_bank }}</strong> <br>
				</p>
			</td>
		</tr>
	</table>

	<p>
		Untuk konfirmasi silahkan klik tombol dibawah ini atau klik link berikut
		<a href="{{ env('VL_DASHBOARD') }}transaction/detail/{{ $data->reference }}">{{ env('VL_DASHBOARD') }}transaction/detail/{{ $data->reference }}</a> <br> <br>
		<button>
			<a href="{{ env('VL_DASHBOARD') }}transaction/detail/{{ $data->reference }}"" target="_blank" style="display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;> <span style="font-size:12px;line-height:24px;"><strong>KONFIRMASI</strong></span> </a>
		</button>
	</p>
</body>
</html>