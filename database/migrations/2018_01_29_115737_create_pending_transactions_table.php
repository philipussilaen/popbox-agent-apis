<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,false);
            $table->string('type',64)->nullable();
            $table->string('reference',256)->nullable();
            $table->integer('transaction_id_reference',false,true)->nullable();
            $table->integer('cart_id',false,true)->nullable();
            $table->text('description')->nullable();
            $table->integer('total_price')->nullable();
            $table->string('status',64)->nullable();
            $table->dateTime('expired_date')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_transactions');
    }
}
