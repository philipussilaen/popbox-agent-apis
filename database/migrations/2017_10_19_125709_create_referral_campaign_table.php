<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->text('description')->nullable();
            $table->string('type',50);
            $table->string('code',50)->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->smallInteger('status')->default(0);
            $table->integer('from_amount')->default(0);
            $table->integer('to_amount')->default(0);
            $table->integer('expired');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_campaigns');
    }
}
