<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVaTypeAndNumberToPaymentDokuVaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_doku_va', function (Blueprint $table) {
            $table->string('va_type',100)->nullable()->after('payments_id');
            $table->string('va_number',150)->nullable()->after('va_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_doku_va', function (Blueprint $table) {
            $table->dropColumn('va_type');
            $table->dropColumn('va_number');
        });
    }
}
