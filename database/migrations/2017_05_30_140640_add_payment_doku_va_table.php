<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentDokuVaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_doku_va', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payments_id',false,false);
            $table->string('permata_va',45)->nullable();
            $table->string('alfamart_va',45)->nullable();
            $table->string('sinarmas_va',45)->nullable();
            $table->string('status',20)->default('WAITING');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payments_id')->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_doku_va');
    }
}
