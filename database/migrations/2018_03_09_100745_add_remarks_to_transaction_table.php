<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarksToTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->text('remarks')->nullable()->after('status');
        });
        Schema::table('transaction_items', function (Blueprint $table) {
            $table->text('remarks')->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('remarks');
        });
        Schema::table('transaction_items', function (Blueprint $table) {
            $table->dropColumn('remarks');
        });
    }
}
