<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commission_schema_id');
            $table->integer('available_parameter_id',false,true);
            $table->string('operator',25);
            $table->text('value');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('commission_schema_id')->references('id')->on('commission_schemas');
            $table->foreign('available_parameter_id')->references('id')->on('available_parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_rules');
    }
}
